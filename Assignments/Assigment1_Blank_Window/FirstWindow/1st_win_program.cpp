/*
	Algorithm :

	1. Include headers & other dependancies : 3 Libs - user32 , kernel32, gdi32
	2. Declare & Define Callback Proc
	3. Define Entry-Point Function
	4. Create Required Struct for Window Generation
	5. Create & Define class of Custom Window : 12 Member init
	6. Register that class to OS
	7. Create Actual windows using above class
	8. Show it to OS / User for interaction & update it
	9. Message Loop - Handling all msgs
   10. Return with current event/status to WndProc / OS / WM
   11. Define callback & Handle occured events in Switch Case.
   12. Return Unhandled msgs to Desktop's Hidden Window.

*/

// 1.
#include <windows.h>

// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

//2. Hollywood Principal
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{
	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC_WINDOW_1"); // name of class 
    TCHAR szWindowCaption[] = TEXT("Tejas's 1st Window");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_SHIELD); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_HAND);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);

	wndclass.lpszClassName = szAppName; // Class Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// handle Instance of current application
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg); //from here OS call's our callback function ie WinProc in this application
	}

	// 10
	return((int)msg.wParam);
}

// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b
	switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		MessageBox(NULL, TEXT("Window Created!"), TEXT("Tejas's 1st Window"), MB_OK);
		break;

		// Last msg
	case WM_DESTROY:
		MessageBox(NULL, TEXT("Window Destroyed"), TEXT("Tejas's 1st Window"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FAILURE 0
#define SUCCESS	1
#define MEMORY_ALLOCATION_FAILURE 2
#define LIST_EMPTY 3

struct date{
	int dd;
	int mm;
	int yyyy;
};

typedef enum{FALSE=0,TRUE} bool;

//This one chage is necessesaty for chaning the data type

typedef struct date type_t ;
//typedef int type_t;


typedef type_t data_t;
typedef int status_t;


typedef struct node
{
	data_t * p_data;
	struct node * next;
	struct node * prev;	
} list_t, node_t;

//Interface functions

list_t * create_list(void);
status_t push(list_t * p_list, data_t * p_data);
status_t top(list_t * p_list,data_t ** pp_data);
status_t pop(list_t * p_list,data_t ** pp_data);
bool is_list_empty(list_t * p_list);
void display_list(list_t * p_list,char * message);
status_t destroy_list(list_t ** pp_list);
void print_data(data_t * in_data,bool LINK_FLAG);

//Auxilary Functions

void generic_insert(node_t * p_beg,node_t * p_mid,node_t * p_end);
void generic_delete(node_t * p_node);
node_t * get_list_node(data_t * p_data);

int main(int argc, char *argv[])
{
	list_t * p_list=create_list();
	data_t * data;
	status_t status;
	
	
	data_t d1={15,8,1947};
	data_t d2={26,2,1991};
	data_t d3={ 22,07,2021};
	
	
	push(p_list,&d1);
	push(p_list,&d2);
	push(p_list,&d3);
	
	display_list(p_list,"Displayinf List of data lements struct date \n");
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	printf("\n\nTOP() and POP() status after list is completely empied\n");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	
	status=pop(p_list,&data);
	printf("POP Status : %d\n ",status);
	
	destroy_list(&p_list);
	if(p_list==NULL)
		puts("List destroyed successfully");
	else
		puts("Issuei in list destroying");
	
	
		
	/*	
	data_t d1=10;
	data_t d2=20;
	data_t d3=30;
	
	
	push(p_list,&d1);
	push(p_list,&d2);
	push(p_list,&d3);
	
	display_list(p_list,"Displayinf List of data lements struct date \n");
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	puts("Data after TOP");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	print_data(data,FALSE);
	puts("Data after POP");
	status=pop(p_list,&data);
	printf("POP	Status : %d\n ",status);
	print_data(data,FALSE);
	
	printf("\n\nTOP() and POP() status after list is completely empied\n");
	status=top(p_list,&data);
	printf("TOP	Status : %d\n ",status);
	
	status=pop(p_list,&data);
	printf("POP Status : %d\n ",status);
	
	destroy_list(&p_list);
	if(p_list==NULL)
		puts("List destroyed successfully");
	else
		puts("Issue in list destroying");
	
	*/			
			
												
		exit(EXIT_SUCCESS);
}



list_t * create_list(void)
{
	list_t * p_list=NULL;
	p_list=(list_t *)malloc(sizeof(list_t));
	if(p_list == NULL)
		return (NULL);
	//create dummy node
	p_list->p_data=NULL;
	p_list->next=p_list;
	p_list->prev=p_list;
	return(p_list);
}


status_t push(list_t * p_list, data_t * p_data)
{
	node_t * p_node=get_list_node(p_data);
	generic_insert(p_list->prev,p_node,p_list);
	return(SUCCESS);	
}

status_t top(list_t * p_list,data_t ** pp_data)
{
	if(is_list_empty(p_list))
		return(LIST_EMPTY);
	*pp_data=p_list->prev->p_data;
	return(SUCCESS);	
}

status_t pop(list_t * p_list,data_t ** pp_data)
{
	if(is_list_empty(p_list))
		return(LIST_EMPTY);
	*pp_data=p_list->prev->p_data;
	generic_delete(p_list->prev);
	return(SUCCESS);	
}

bool is_list_empty(list_t * p_list)
{
	if(p_list->next == p_list && p_list->prev== p_list)
		return(TRUE);
	else
		return(FALSE);
}

void display_list(list_t * p_list,char * message)
{
	if(message != NULL)
	{
		printf("%s",message);	
	}	
	
	
	node_t * p_run = p_list->next;
	
	printf("[START]<->");
	while(p_run != p_list)
	{
		print_data(p_run->p_data,TRUE);
		p_run=p_run->next;
	}
	puts("[END]");
}

status_t destroy_list(list_t ** pp_list)
{
	node_t * p_run=(*pp_list)->next;
	node_t * p_run_next=NULL;
	while(p_run != *pp_list)
	{
		p_run_next=p_run->next;
		generic_delete(p_run);
		p_run=p_run_next;
	}	
	*pp_list=NULL;
	return (SUCCESS);
}


//Auxilary Functions

void generic_insert(node_t * p_beg,node_t * p_mid,node_t * p_end)
{
	p_mid->next=p_end;
	p_mid->prev=p_beg;
	p_beg->next=p_mid;
	p_end->prev=p_mid;
}

void generic_delete(node_t * p_node)
{
	p_node->next->prev=p_node->prev;
	p_node->prev->next=p_node->next;
	//free the data
	// free(p_node->p_data);
	//free the list from our DCLL
	free(p_node);
}

node_t * get_list_node(data_t * p_data)
{
	node_t * p_node=NULL;
	p_node=(node_t *)malloc(sizeof(node_t));
	if(p_node == NULL)
		return (NULL);
	
	p_node->p_data=p_data;
	p_node->next=NULL;
	p_node->prev=NULL;	
	return (p_node);
}


void print_data(data_t * in_data,bool LINK_FLAG)
{
	if(LINK_FLAG == TRUE)
		printf("[%d-%d-%d]<->",in_data->dd,in_data->mm,in_data->yyyy );
	else
		printf("[%d-%d-%d]\n",in_data->dd,in_data->mm,in_data->yyyy );
}


/*
void print_data(data_t * in_data,bool LINK_FLAG)
{
	if(LINK_FLAG == TRUE)
		printf("[%d]<->", *in_data);
	else
		printf("[%d]\n", *in_data);
}
*/



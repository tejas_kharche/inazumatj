//#############################
//      Name : Tejas										#
//	  MSTC													 #
//	  Assignment: STACK USING DCLL	#
//#############################

#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 									   1
#define FAILURE 										 0
#define STACK_EMPTY 							2
#define NULL_POINTER_EXCEPTION 	3
#define assert(CONDITION) do{\
													if(!(CONDITION))\
													{\
														puts("ASSERT MACRO:   CONDITION NOT SATISFIED");\
														exit(1);\
													}\
													}while(0)

typedef enum{FALSE=0,TRUE} bool;

typedef struct node{
	int data;
	struct node * next;
	struct node * prev;
}node_t,stack_t;

typedef int status_t;
typedef int data_t;
typedef int len_t;
 


//////// INTERNAL FUNCTIONS	//////////

status_t genericInsert(stack_t * p_beg, stack_t * p_mid, stack_t * p_end);
status_t genericDelete(stack_t * p_node);
void * xmalloc(size_t size_in_byte);

node_t * createNode(data_t in_data);


//////// INTERFACE FUNCTION	//////////
stack_t * createStack();

bool isStackEmpty(stack_t * p_stack);

status_t push(stack_t * p_stack,data_t in_data);

status_t pop(stack_t * p_stack, data_t * p_out_data);

status_t top(stack_t * p_stack, data_t * p_out_data);

status_t displayStack(stack_t * p_stack, char * message);

status_t destroyStack(stack_t **p_stack);

int main(int argc, char *argv[])
{
	status_t status;
	data_t data;
	stack_t * p_stack=createStack();
	int i;
	
	for(i=1 ;i<=5;i++)
	{
		push(p_stack, i*10);
		displayStack(p_stack,"DISPLAY Stack after PUSH()\n");
	}
	
	
	
	displayStack(p_stack,"Stack Before POP()\n");
	while(!isStackEmpty(p_stack))	
	{	
		status=pop(p_stack,&data);
		if(status== SUCCESS)
		{
			printf("POP ELEMENT : %d  \n",data);
			displayStack(p_stack,"Stack After POP()\n");
		}
		puts("#############################################################");
		status=top(p_stack,&data);
		if(status==SUCCESS)
		{
			printf(" Top ELEMENT : %d \n ",data);
			displayStack(p_stack,"Stack After TOP()\n");
		}
	}
	
	destroyStack(&p_stack);
	
	//jsut checkigng if the queue is destroyed or  not
	if(p_stack == NULL)
		printf("STACK DESTROYED");
	else
		printf("STACK NOT DESTROYED");
		
}

status_t genericInsert(stack_t * p_beg, stack_t * p_mid, stack_t * p_end)
{
	p_mid->next=p_end;
	p_mid->prev=p_beg;	
	p_beg->next=p_mid;
	p_end->prev=p_mid;
			
	return (SUCCESS);	
}

status_t genericDelete(stack_t * p_node)
{
	p_node->prev->next=p_node->next;
	p_node->next->prev=p_node->prev;
	p_node->next=NULL;
	p_node->prev=NULL;
	free(p_node);
	p_node=NULL;		
	return (SUCCESS);
}


bool isStackEmpty(stack_t * p_stack)
{
 	if (p_stack->next == p_stack && p_stack->prev == p_stack)
 		return (TRUE);
 	else
 		return (FALSE); 
}

void * xmalloc(size_t size_in_byte)
{
	void * p=NULL;
	p=malloc(size_in_byte);
	assert(p != NULL);
	return (p);	
}

stack_t * createStack()
{
	node_t * p_stack=NULL;
	p_stack=(stack_t *)xmalloc(sizeof(stack_t));
	p_stack->data=0;
	p_stack->next=p_stack;
	p_stack->prev=p_stack;
	
	return(p_stack);
}

node_t * createNode(data_t in_data)
{
	node_t * p=NULL;
	p=(node_t *)xmalloc(sizeof(node_t));
	p->data=in_data;
	p->prev=NULL;
	p->next=NULL;
	
	return(p);
}


status_t push(stack_t * p_stack,data_t in_data)
{
		if(p_stack==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
		}
	genericInsert(p_stack->prev,createNode(in_data),p_stack);	
	return(SUCCESS);
}

status_t pop(stack_t * p_stack, data_t * p_out_data)
{
	if(p_stack==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isStackEmpty(p_stack)){
		return(STACK_EMPTY);
	}
		
	*p_out_data=p_stack->prev->data;
	genericDelete(p_stack->prev);
	return(SUCCESS);
}


status_t top(stack_t * p_stack, data_t * p_out_data)
{
	if(p_stack==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isStackEmpty(p_stack)){
		return(STACK_EMPTY);
	}
		
	*p_out_data=p_stack->prev->data;
	return(SUCCESS);
}

status_t destroyStack(stack_t **p_stack)
{
	if(*p_stack==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	node_t * p_run=(*p_stack)->next;
	node_t * p_run_next=NULL;
	while(p_run != *p_stack)
	{
		p_run_next=p_run->next;
		genericDelete(p_run);
		p_run=p_run_next;
	}
	
	free(*p_stack);
	*p_stack=NULL;
	
	return(SUCCESS);
	
}

status_t displayStack(stack_t * p_stack, char * message)
{
	if(p_stack == NULL){
		return(NULL_POINTER_EXCEPTION);	
	}
	
	if(message != NULL){
		printf("%s",message);
	}
	
	node_t * p_run=p_stack->next;
	printf("[START]<->");
	while(p_run != p_stack)
	{
		printf("[%d]<->",p_run->data);
		p_run=p_run->next;
	}
	puts("[END]");
	
	return(SUCCESS);
}
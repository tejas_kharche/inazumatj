//######### ############# 
//	Author : Tejas Kharche        #
//###################### 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 1
#define FAILURE 0
#define STACK_EMPTY 2

#define assert(CONDITION)  do{\
														if(!(CONDITION))\
															exit(1);\
							                           }while(0)

struct Vector
{
	int * Array;
	int  ArraySize;
	int TopIndex;
};

typedef struct Vector stack_t;
typedef enum{FALSE=0,TRUE} bool;
typedef int status_t;
typedef int data_t;


stack_t * createStack(size_t stack_size);
status_t push(stack_t * p_stack,data_t in_data);
status_t pop(stack_t * p_stack, data_t * out_data);
status_t top(stack_t *p_stack,data_t * out_data);
status_t destroyStack(stack_t * p_stack);
void displayStack(stack_t * p_stack, char * message);



void * xmalloc(void * p,size_t size_in_bytes);
bool isStackEmpty(stack_t * p_stack);

int main(int argc, char *argv[])
{
	stack_t * p_stack = createStack(2);
	data_t out_data;
	status_t status;
		int i;

	puts("##### PUSH #####");

	for (i=1 ; i<= 5;i++)
	{
		printf("\nInside for loop TOP : %d\n",p_stack->TopIndex);
		push(p_stack,i*10);
		printf("\n Display Stack after pushing %d \n",i*10);
		//printf("Inside for loop TOP : %d",p_stack->TopIndex);
		displayStack(p_stack,NULL);
		
	}
	
	printf("\n OUTside for loop TOP : %d\n",p_stack->TopIndex);
	
/*
	puts("##### TOP #####");
	
	for(i=1 ; i<=10 ;i++)
	{
		status=top(p_stack,&out_data);
		printf("Status : %d  |  Top :  %d | Item : %d \n",status,p_stack->TopIndex, out_data);
	}
	*/
	
	puts("##### POP #####");
			
	while(!isStackEmpty(p_stack))
	{
		status=pop(p_stack,&out_data);
		printf("Status : %d  |  Top :  %d | Item : %d \n",status,p_stack->TopIndex, out_data);
		printf("\n Display Stack after POP %d \n",out_data);
		displayStack(p_stack,NULL);
		
	}
	
	
}

void * xmalloc(void * p,size_t size_in_bytes)
{
	void * p_new = NULL;
	p_new=realloc(p,size_in_bytes);
	//assert(p_new != NULL);
	return p_new;	
}


stack_t * createStack(size_t stack_size)
{
	
	stack_t * p_stack=NULL;
	p_stack=(stack_t *)xmalloc(p_stack,sizeof(stack_t));
	p_stack->Array=(data_t *)xmalloc(p_stack->Array, stack_size * sizeof(data_t) );
	memset(p_stack->Array, 0, stack_size * sizeof(data_t) );
	p_stack->ArraySize=stack_size;
//	p_stack->TopIndex=0;

	p_stack->TopIndex=-1;
	return (p_stack);
}


bool isStackEmpty(stack_t *  p_stack)
{
	//if(p_stack->TopIndex == 0)
		if(p_stack->TopIndex == -1 )
		return (TRUE);
	else
		return (FALSE);
}

status_t push(stack_t * p_stack,data_t in_data)
{
	if(p_stack->ArraySize-1 == p_stack->TopIndex)
	{
		puts("Pushed Elements is OUT OF Intialized Stack Bounds");
		
		p_stack->ArraySize++;
		p_stack->TopIndex++;
		p_stack->Array=(data_t *)realloc(p_stack->Array,p_stack->ArraySize*sizeof(data_t));			
		assert(p_stack->Array != NULL);
		//p_stack->Array[p_stack->TopIndex-1]=in_data;
		p_stack->Array[p_stack->TopIndex]=in_data;
		
	}
	else if ( p_stack->TopIndex < p_stack->ArraySize){
		puts("Pushed Elements is WITHIN Intialized Stack Bounds");		
		
		p_stack->TopIndex++;
		//p_stack->Array[p_stack->TopIndex-1]=in_data;
		p_stack->Array[p_stack->TopIndex]=in_data;
		//p_stack->Array=(stack_t)xrealloc(p_stack->Array,p_stack->TopIndex*sizeof(data_t));		
	}	
	return (SUCCESS);	
}

status_t pop(stack_t * p_stack, data_t * out_data)
{
	if(isStackEmpty(p_stack)){
		return (STACK_EMPTY); 	
	}
	
	
	*out_data=p_stack->Array[p_stack->TopIndex];		
	
	p_stack->TopIndex--;
	p_stack->ArraySize--;
	
	
	p_stack->Array=(data_t *)realloc(p_stack->Array,p_stack->ArraySize* sizeof(data_t));
	//assert(p_stack->Array != NULL);
	return (SUCCESS);		
}

status_t top(stack_t *p_stack,data_t * out_data)
{
	if(isStackEmpty(p_stack)){
		return (STACK_EMPTY); 
	}else{
	
	//*out_data=p_stack->Array[p_stack->TopIndex-1];
	
	*out_data=p_stack->Array[p_stack->TopIndex];
	
	return( SUCCESS);		
	}
}

void displayStack(stack_t * p_stack, char * message)
{
	if (message != NULL)
	{
		printf("%s",message);
	}
	puts("TOP");
	int i=0;

	for(i=0; i <= p_stack->TopIndex ; i++ )
	{
		printf("[%d]\n",p_stack->Array[i]);
	}
	
	puts("END");
}


status_t destroyStack(stack_t * p_stack)
{
 	if( p_stack != NULL)
 	{
 		free(p_stack->Array);
 		p_stack->Array=NULL;
 		free(p_stack);
 		p_stack=NULL;
 		return(SUCCESS);
 	}
 	return(FAILURE);	
}
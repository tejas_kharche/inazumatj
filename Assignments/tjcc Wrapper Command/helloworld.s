	.arch armv8-a
	.file	"helloworld.c"
	.text
	.section	.rodata
	.align	3
.LC0:
	.string	"Hello World"
	.align	3
.LC1:
	.string	"This is compiled using  my Wrapper CMD"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
.LFB6:
	.cfi_startproc
	stp	x29, x30, [sp, -16]!
	.cfi_def_cfa_offset 16
	.cfi_offset 29, -16
	.cfi_offset 30, -8
	mov	x29, sp
	adrp	x0, .LC0
	add	x0, x0, :lo12:.LC0
	bl	puts
	adrp	x0, .LC1
	add	x0, x0, :lo12:.LC1
	bl	puts
	mov	w0, 0
	bl	exit
	.cfi_endproc
.LFE6:
	.size	main, .-main
	.ident	"GCC: (Debian 9.3.0-14) 9.3.0"
	.section	.note.GNU-stack,"",@progbits

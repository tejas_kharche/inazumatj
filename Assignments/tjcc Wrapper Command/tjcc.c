// @Author : Tejas Kharche///
// Basic Gcc like wrapper command implementation//

#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<getopt.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<errno.h>

typedef enum{FALSE=0,TRUE} bool;

typedef int flag_t;
typedef int status_t;


status_t assemble(char * s_file_name,char * o_file_name); 
status_t compile(char * c_file_name,char * o_file_name); 
status_t compile_link_default(char * c_file_name,char * o_file_name);
status_t preprocess(char * c_file_name,char * o_file_name);
status_t linking(char * object_file_name, char * o_file_name, char * lib_file_name,char * entry_function_name);
void display_help();
void display_version();

int main(int argc,char *argv[],char *envp[])
{
	int s_flag=FALSE;
	int dl_flag=FALSE;
	int o_flag=FALSE;  //flags
 int obj_flag=FALSE;
 int preprocess_flag=FALSE;
 int entry_func_flag=FALSE;
 int input_flag=FALSE;

 int selected_option;
	int selected_option_index=0;
 char * obj_file_name=NULL;
 char * asm_file_name=NULL;
 //char * c_file_name=NULL;
 char * lib_file_name=NULL;
 char * output_file_name=NULL;
 char * entry_func_name=NULL;
 char * input_file_name=NULL;
 
	struct option mygcc_options[]={
		{"compile-only",no_argument,0,'c'},
		{"output",required_argument,0,'o'},
		{"assemble-only",no_argument,0,'s'},
	 	{"preprocess-only",no_argument,0,'E'},
	 	{"dynamic-linker",required_argument,0,'l'},
	 	{"input",required_argument,0,'i'},
	 	{"save-temps",no_argument,0,'t'},
	 	{"version",no_argument,0,'v'},
	 	{"help",no_argument,0,'h'}
	};


	if(argc == 1)
	{
	   puts("No Input C file is given");
	   //display_help();
	   exit(EXIT_SUCCESS);
	}
    /*
	if( argc == 2)
	{
	   compile_link_default(argv[1],"a.out");
	}
     */

	while((selected_option=getopt_long(argc,argv,"hvcsEo:e:l:i:",mygcc_options,&selected_option_index)) != -1)
	{
	    switch((char)selected_option)
	    {
		    case 's':
			    printf("-s | -assemble-only is choosen\n");
			    s_flag=TRUE;
			    //asm_file_name=optarg;
			    break;
			    
		    case 'o':
			    printf("-o | -output is selected\n");
			    o_flag=TRUE;
			    output_file_name=optarg;
			    break;
			   
                    case 'h':
			   display_help();
			   exit(EXIT_SUCCESS);
			   break;
			    
	            case  'v':
			   display_version();
			   exit(EXIT_SUCCESS);
			   break;
		    
		    case 'l':
			    printf("-dynamic-linker ie selected\n");
		     dl_flag=TRUE;
	      lib_file_name=optarg;
	      break;
	      
	     case 'c':
	       printf("-c | -compile-only is selected\n"); 
	       obj_flag=TRUE;
	       //obj_file_name=optarg
	       break;
	       
	     case 'E':
	        printf("-E | -preprocess is selected\n"); 
	        preprocess_flag=TRUE;
	        break;
	        
	     case 'e':
	        printf("-e is selected\n");
	        entry_func_flag=TRUE;
	        entry_func_name=optarg;
	        break;
	     
	     case 'i':
	        printf("-i | -input is selected\n");
	        input_flag=TRUE;
		input_file_name=optarg;
    		break;
    		
 		 default:
			     display_help();	
			     exit(EXIT_FAILURE);
	    }
	}
 

//default
if( argc == 2)
{
   compile_link_default(argv[1],"a.out");
}

 //Create Assembly file from c file
 if(s_flag==TRUE && o_flag==TRUE)
 {
     assemble(input_file_name,output_file_name);
 }
 //Create object file from c file
 if(obj_flag==TRUE && o_flag==TRUE)
 {
     compile(input_file_name,output_file_name);
 }
 
 if(preprocess_flag==TRUE && o_flag==TRUE)
 {
	preprocess(input_file_name,output_file_name);
 }
 
 if(dl_flag==TRUE && o_flag==TRUE)
 {
     if(entry_func_flag==FALSE)
     {
         linking(input_file_name,output_file_name,lib_file_name,"main");
     }
     else
     {
         linking(input_file_name,output_file_name,lib_file_name,entry_func_name);
     }
     
 }
 

	return(EXIT_SUCCESS);
}

void display_help()
{
	printf("mygcc Usage:\n o | output\n s | assemble\n");
}


status_t assemble(char * c_file_name,char * o_file_name)
{	pid_t pid;
	status_t status;
	int ret_wait;
	char * cmd_args[]={"cc","-S","-o",o_file_name,c_file_name,(char*)NULL};
	
	pid=fork();
	if(pid==0)  //task done by child process
	{
	    status=execvp(cmd_args[0],cmd_args);
	    if(status==-1)
	    {  
		fprintf(stderr,"as : %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	    }
	}
	else
	{
		wait(&ret_wait);
	}
	
	WEXITSTATUS(ret_wait);
	//return(*(((char*)&ret_wait)+1));
}


/*
status_t assemble(char * s_file_name,char * o_file_name)
{	pid_t pid;
	status_t status;
	int ret_wait;
	char * cmd_args[]={"as","-o",o_file_name,s_file_name,(char*)NULL};
	
	pid=fork();
	if(pid==0)  //task done by child process
	{
	    status=execvp(cmd_args[0],cmd_args);
	    if(status==-1)
	    {  
		fprintf(stderr,"as : %s\n",strerror(errno));
		exit(EXIT_FAILURE);
	    }
	}
	else
	{
		wait(&ret_wait);
	}
	
	WEXITSTATUS(ret_wait);
	//return(*(((char*)&ret_wait)+1));
}
*/


status_t preprocess(char * c_file_name,char * o_file_name)
{

 	pid_t pid;
	int ret_wait;
	status_t status;
	
        char * cmd_args_v1[]={"cpp",c_file_name,"-o",o_file_name,(char*)NULL};
 	char * cmd_args_v2[]={"cpp",c_file_name,(char *)NULL};
	

 	
         pid=fork();
	 if(pid==0)
	 {
	 	if(o_file_name==NULL)
			status=execvp(cmd_args_v2[0],cmd_args_v2);
		else
			status=execvp(cmd_args_v1[0],cmd_args_v1);
		
		if(status==-1)
		{
			fprintf(stderr,"cpp : %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		wait(&ret_wait);
	}
	
        
	WEXITSTATUS(ret_wait);
	//
}

status_t linking(char * object_file_name, char * o_file_name, char * lib_file_name,char * entry_function_name)
{
	pid_t pid;
	int ret_wait;
	status_t status;
	char * cmd_args[]={"ld",object_file_name,"-o",o_file_name,"-lc","-dynamic-linker",lib_file_name,"-e",entry_function_name,(char *)NULL};
	
	///lib/ld-linux-*.so.*
	pid=fork();

	if(pid == 0)
	{  
		status=execvp(cmd_args[0],cmd_args);
		if(status==-1)
		{
			fprintf(stderr,"ld :%s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		wait(&ret_wait);
	}
	WEXITSTATUS(ret_wait);
}


status_t compile_link_default(char * c_file_name,char * o_file_name)
{
	pid_t pid;
	int ret_wait;
	status_t status;
	char * cmd_args[]={"cc",c_file_name,"-o",o_file_name,(char *)NULL};

	pid=fork();
	if(pid==0)
	{
		status=execvp(cmd_args[0],cmd_args);
		if(status==-1)
		{
			fprintf(stderr,"cc : %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
	}else
	{
		wait(&ret_wait);
	}
	
	WEXITSTATUS(ret_wait);
}


void display_version()
{
    puts("Vestion 1.0.0");
}


status_t compile(char * c_file_name,char * o_file_name)
{
	pid_t pid;
	int ret_wait;
	status_t status;
	char * cmd_args[]={"cc","-c",c_file_name,"-o",o_file_name,(char *)NULL};

	pid=fork();
	if(pid==0)
	{
		status=execvp(cmd_args[0],cmd_args);
		if(status==-1)
		{
			fprintf(stderr,"cc : %s\n",strerror(errno));
			exit(EXIT_FAILURE);
		}
	}else
	{
		wait(&ret_wait);
	}
	
	WEXITSTATUS(ret_wait);
}


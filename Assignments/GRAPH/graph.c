#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>


#define SUCCESS 1
#define FAILURE 0
#define NULL_POINTER_EXCEPTION 2
#define LIST_EMPTY 4
#define VERTEX_ALREADY_EXIST  3
#define EDGE_ALREADY_EXIST 4 
#define EDGE_DOES_NOT_EXIST 5
#define VERTEX_DOES_NOT_EXIST 6
#define INVALID_VERTEX  7
#define INVALID_EDGE 8


typedef int data_t;
typedef int  status_t;
typedef int vertex_t;
 
typedef enum{FALSE=0,TRUE} bool;


typedef struct h_node
{
	vertex_t vertex;
	struct h_node * prev;
	struct h_node * next;		
}h_list_t,h_node_t;

typedef struct v_node
{
	vertex_t vertex;
	h_list_t * ph_list;
	struct v_node * prev;
	struct v_node * next;		
}v_list_t,v_node_t;


typedef struct graph
{
	v_list_t * pv_list;
	int nr_vertex;
	int nr_edges;
}graph_t;

typedef struct edge
{
	vertex_t v_start;
	vertex_t v_end;
}edge_t;

// Internal functions for hlist//

h_node_t * get_hnode(data_t in_data);
h_node_t * search_hnode(h_list_t * ph_list,data_t e_data);
void generic_insert_hlist(h_node_t* beg,h_node_t * mid,h_node_t * end);
void generic_delete_hlist(h_node_t * p_node);

// Interface function of hlist

h_list_t  * create_hlist();
status_t  destroy_hlist(h_list_t  ** ph_list);
status_t insert_end_hlist(h_list_t * ph_list,data_t in_data);



//_________________________________________________________________________________________//



// Internal functions for vlist//

v_node_t * get_vnode(data_t in_data);
v_node_t * search_vnode(v_list_t * pv_list,data_t e_data);
void generic_insert_vlist(v_node_t* beg,v_node_t * mid,v_node_t * end);
void generic_delete_vlist(v_node_t * p_node);

// Interface function of vlist

v_list_t  * create_vlist();
status_t  destroy_vlist(v_list_t  ** pv_list);
status_t insert_end_vlist(v_list_t * pv_list,data_t in_data);


// Interface Functions of Graph //

graph_t * create_graph(void);
status_t destroy_graph(graph_t ** p_graph);
status_t add_vertex(graph_t * p_graph,vertex_t in_vertex);
status_t remove_vertex(graph_t * p_graph,vertex_t  e_vertex);

status_t add_edge(graph_t * p_graph,int v_start,int v_end);
status_t remove_edge(graph_t * p_graph,int v_start,int v_end);

status_t display_graph(graph_t *p_graph,char * msg);

int main(int argc, char *argv[])
{
	status_t status=-1;
	vertex_t VERTICES[]={1,2,3,4};
	edge_t EDGES[]={{1,2},{2,3},{3,4},{4,1},{2,4}};
	
	graph_t *p_graph=create_graph(); 
	
	size_t total_vertex=sizeof(VERTICES)/sizeof(VERTICES[0]);
	size_t total_edges=sizeof(EDGES)/sizeof(EDGES[0]);
	
	int i=0;
	
	//adding vertices
	for(i=0;i<total_vertex;i++)
	{
		add_vertex(p_graph,VERTICES[i]);	
	}
	
	//Adding edges
	for(i=0;i<total_edges;i++)
	{
		add_edge(p_graph,EDGES[i].v_start,EDGES[i].v_end);	
	}

	display_graph(p_graph,"Displaying Graph\n");	

	status=remove_edge(p_graph,2,4);
	printf("remove_edge (2,4) STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after removing edge (2,4)\n");	
	
	/*
	
	status=remove_edge(p_graph,1,3);
	printf("remove_edge (1,3) STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after removing edge (1,3)\n");	
*/
	status=remove_vertex(p_graph,4);
	printf("remove_vertex 4 STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after removing vertex 4\n");	

	status=remove_vertex(p_graph,2);
	printf("remove_vertex 2 STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after removing vertex 2\n");	


	status=add_vertex(p_graph,2);
	printf("add_vertex 2 STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after adding vertex 2\n");
	
	status=add_vertex(p_graph,4);
	printf("add_vertex 4 STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after adding vertex 4\n");
	
	status=add_edge(p_graph,1,3);
	printf("add_edge (1,3)  STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after adding edge (1,3)\n");

	status=add_edge(p_graph,4,2);
	printf("add_edge (4,2)  STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after adding edge (4,2)\n");

	status=add_edge(p_graph,4,3);
	printf("add_edge (4,3)  STATUS : %d \n",status);
	display_graph(p_graph,"Displaying Graph after adding edge (4,3)\n");

	destroy_graph(&p_graph);
	
	if(p_graph == NULL)
		puts("Graph Deleted");
}


h_node_t * get_hnode(data_t in_data)
{
	h_node_t * p_node=(h_node_t *)calloc(1,sizeof(h_node_t));
	if(p_node == NULL)
		return(NULL);
		
	p_node->vertex=in_data;
	p_node->prev=NULL;
	p_node->next=NULL;
	return(p_node);
}

h_node_t * search_hnode(h_list_t * ph_list,data_t e_data)
{
	if(ph_list == NULL)  ////if the pointer to the list  is NULL do nott check and then NULL willbe returned
		return(NULL);
		
	h_node_t * p_run=ph_list->next;
	
	while(p_run != ph_list)
	{
		if(p_run->vertex == e_data)
			return(p_run);//if the data is found in the  list then it's  corresponding pointer  will be returned'
			
		p_run=p_run->next;
	}
	
	return(NULL); //if the verted is not present then NULL willbe returned	
}

void generic_insert_hlist(h_node_t* beg,h_node_t * mid,h_node_t * end)
{
	beg->next=mid;
	end->prev=mid;
	mid->next=end;
	mid->prev=beg;	
}

void generic_delete_hlist(h_node_t * p_node)
{
	p_node->prev->next=p_node->next;
	p_node->next->prev=p_node->prev;
	
	
	free(p_node);
	p_node=NULL;
}

// Interface function of hlist

h_list_t  * create_hlist()
{
	h_list_t * ph_list=NULL;
	ph_list=(h_list_t *)calloc(1,sizeof(h_list_t));
	if(ph_list == NULL)
		return(NULL);
		
	ph_list->vertex=0;
	ph_list->next=ph_list;
	ph_list->prev=ph_list;
	return (ph_list);
}

status_t  destroy_hlist(h_list_t  ** ph_list)  // do some changes in there do not delete the dummy node
{
	if(*ph_list == NULL)
		return(NULL_POINTER_EXCEPTION);
		
	h_node_t * p_run=(*ph_list)->next;
	h_node_t * p_run_next=(*ph_list)->next;
	
	while(p_run != *ph_list)
	{
		p_run_next=p_run->next;
		free(p_run);
		p_run=NULL;
		p_run=p_run_next;
	}
	
	free(*ph_list); //free dummy node
	*ph_list=NULL; //make the pointer as NULL
	return(SUCCESS);
}

status_t insert_end_hlist(h_list_t * ph_list,data_t in_data)
{
	if (ph_list == NULL)
		return(NULL_POINTER_EXCEPTION);
		
	generic_insert_hlist(ph_list->prev,get_hnode(in_data),ph_list);
	return( SUCCESS);
}



//_________________________________________________________________________________________//



// Internal functions for vlist//

v_node_t * get_vnode(data_t in_data)
{
	v_node_t * p_node=(v_node_t *)calloc(1,sizeof(v_node_t));
	if(p_node == NULL)
		return(NULL);
	
	p_node->ph_list=create_hlist();
	if(p_node->ph_list == NULL)
		return(NULL);  //hlist  formation has failed
		
	p_node->vertex=in_data;
	p_node->prev=NULL;
	p_node->next=NULL;
	//p_node->ph_list=NULL;
	
	
	return(p_node);
	
}


v_node_t * search_vnode(v_list_t * pv_list,data_t e_data)
{
		
	if(pv_list == NULL)  ////if the pointer to the list  is NULL do nott check and then NULL willbe returned
		return(NULL);
		
	v_node_t * p_run=pv_list->next;
	
	while(p_run != pv_list)
	{
		if(p_run->vertex == e_data)
			return(p_run);//if the data is found in the  list then it's  corresponding pointer  will be returned'
			
		p_run=p_run->next;
	}
	
	return(NULL); //if the verted is not present then NULL willbe returned	
}


void generic_insert_vlist(v_node_t* beg,v_node_t * mid,v_node_t * end)
{
	beg->next=mid;
	end->prev=mid;
	mid->next=end;
	mid->prev=beg;	
}

void generic_delete_vlist(v_node_t * p_node)
{
	p_node->prev->next=p_node->next;
	p_node->next->prev=p_node->prev;

	free(p_node);
	p_node=NULL;	
}

// Interface function of vlist

v_list_t  * create_vlist()
{
	v_list_t * pv_list=NULL;
	pv_list=(v_list_t *)calloc(1,sizeof(v_list_t));
	if(pv_list == NULL)
		return(NULL);
	pv_list->vertex=0;
	pv_list->next=pv_list;
	pv_list->prev=pv_list;
	return (pv_list);

}

status_t  destroy_vlist(v_list_t  ** pv_list)  // do some changes in there do not delete the dummy node
{
		if(*pv_list == NULL)
		return(NULL_POINTER_EXCEPTION);
		
	v_node_t * p_run=(*pv_list)->next;
	v_node_t * p_run_next=(*pv_list)->next;
	
	while(p_run != *pv_list)
	{
		p_run_next=p_run->next;
		free(p_run);
		p_run=NULL;
		p_run=p_run_next;
	}
	
	free(*pv_list); //free dummy node
	*pv_list=NULL; //make the pointer as NULL
	return(SUCCESS);
}

status_t insert_end_vlist(v_list_t * pv_list,data_t in_data)
{
	if (pv_list == NULL)
		return(NULL_POINTER_EXCEPTION);
		
	generic_insert_vlist(pv_list->prev,get_vnode(in_data),pv_list);
	return( SUCCESS);
}


graph_t * create_graph(void)
{
	graph_t *  p_graph=NULL;
	p_graph=(graph_t *)calloc(1,sizeof(graph_t));
	if(p_graph == NULL)
		return(NULL);
	
	p_graph->pv_list=create_vlist();
	
	if(p_graph->pv_list == NULL)
		return(NULL);
		
	p_graph->nr_edges=0;
	p_graph->nr_vertex=0;

	return(p_graph);			
}

status_t destroy_graph(graph_t ** p_graph)
{
	status_t status=-1;
	v_node_t * p_v_run=NULL;
	p_v_run=(*p_graph)->pv_list->next;
	
	while(p_v_run != (*p_graph)->pv_list)
	{
		status=destroy_hlist(&(p_v_run->ph_list));
		if(status != SUCCESS)
		{
			fprintf(stderr,"destroy_hlist() : %s",strerror(errno));
			return(status);
		}
		p_v_run=p_v_run->next;
	}
	
		status=destroy_vlist(&((*p_graph)->pv_list));
		if(status != SUCCESS)
		{
			fprintf(stderr,"destroy_vlist() : %s",strerror(errno));
			return(status);
		}
	
	free(*p_graph);
	*p_graph=NULL;
	//destroy vlist
	
	return(SUCCESS);	
}

status_t add_vertex(graph_t * p_graph,vertex_t in_vertex)
{
	status_t status=-1;
	if(p_graph == NULL)
		return(NULL_POINTER_EXCEPTION);
		
	status=insert_end_vlist(p_graph->pv_list,in_vertex); 
	if(status == SUCCESS)
		return(SUCCESS);
		
	return(FAILURE);
}


status_t remove_vertex(graph_t * p_graph,vertex_t  e_vertex)
{
	//status_t status=-1;
	
	v_node_t * p_v_node=NULL;
	p_v_node=search_vnode(p_graph->pv_list,e_vertex);
	if(p_v_node == NULL)
	{
		return(VERTEX_DOES_NOT_EXIST);
	}
	
	v_node_t * p_v_lookup_node=NULL;
	h_node_t * p_h_lookup_node=NULL;
	
	h_node_t * p_h_run=p_v_node->ph_list->next;
	while(p_h_run != p_v_node->ph_list)
	{
		//Code to remove p_v_run->vertex from adjacent list of 
		
		p_v_lookup_node=search_vnode(p_graph->pv_list,p_h_run->vertex);
		
		if(p_v_lookup_node == NULL)
			return(EDGE_DOES_NOT_EXIST);//this should never happen it means that graph is in consistent
p_h_lookup_node=search_hnode(p_v_lookup_node->ph_list,e_vertex);

if(p_h_lookup_node==NULL)
			return(EDGE_DOES_NOT_EXIST);//this should never happen it means that graph is in consistent
		
		generic_delete_hlist(p_h_lookup_node);
		
		p_h_run=p_h_run->next;
	}	

	generic_delete_vlist(p_v_node);
					
	return(SUCCESS);
}


status_t add_edge(graph_t * p_graph,int v_start,int v_end)
{
	status_t status=-1;
	
	v_node_t * v1=search_vnode(p_graph->pv_list,v_start);
	v_node_t * v2=search_vnode(p_graph->pv_list,v_end);
	
	if(v1 == NULL ||  v2 ==NULL)//This checks if the new edge can be created or not edged vertex should be present in the v_list first then only we can create an edge between them.
	return(VERTEX_DOES_NOT_EXIST);
	
	
	h_node_t * h1=search_hnode(v1->ph_list,v_end);    //check if the v_end is already present in the h_list of vertex v1
	h_node_t * h2=search_hnode(v2->ph_list,v_start);
	
		if(h1 != NULL && h2!=NULL)
			return(EDGE_ALREADY_EXIST);
		
		status=insert_end_hlist(v1->ph_list,v_end);
		if(status != SUCCESS)
			return(FAILURE);
			
		status=insert_end_hlist(v2->ph_list,v_start);
		if(status != SUCCESS)
			return(FAILURE);
			
	return(SUCCESS);
}

status_t remove_edge(graph_t * p_graph,int v_start,int v_end)
{

	v_node_t * v1=search_vnode(p_graph->pv_list,v_start);
	v_node_t * v2=search_vnode(p_graph->pv_list,v_end);
	
	if(v1 == NULL ||  v2 ==NULL)//This checks if the new edge can be created or not edged vertex should be present in the v_list first then only we can create an edge between them.
	return(EDGE_DOES_NOT_EXIST);
	
	
	h_node_t * h1=search_hnode(v1->ph_list,v_end);    //check if the v_end is already present in the h_list of vertex v1
	h_node_t * h2=search_hnode(v2->ph_list,v_start);
	
		if(h1 == NULL || h2 ==NULL)
			return(EDGE_DOES_NOT_EXIST);
		
		generic_delete_hlist(h1);  //remove node h1 from it's  respective list
		generic_delete_hlist(h2); //remove node h2 from it's respective list
		
	return(SUCCESS);
}

status_t display_graph(graph_t *p_graph,char * msg)
{
	
	if(p_graph==NULL)
		return(NULL_POINTER_EXCEPTION);
	
	if(msg != NULL)
		printf("%s",msg);		
	
	v_node_t * p_v_run=p_graph->pv_list->next;
	
	while(p_v_run != p_graph->pv_list)
	{
		
		printf("[%d] ---> [",p_v_run->vertex);
		
		h_node_t * p_h_run=p_v_run->ph_list->next;
		while(p_h_run != p_v_run->ph_list )
		{
			printf("%d  ",p_h_run->vertex);
			p_h_run=p_h_run->next;
		}
		printf("]\n");
		p_v_run=p_v_run->next;
	}
	
	return(SUCCESS);
}

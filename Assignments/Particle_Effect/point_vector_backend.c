#include <stdio.h>
#include <stdlib.h>

//int CLIENT_XArea,CLIENT_YArea;
int CLIENT_XArea=800;
int CLIENT_YArea=800;
int SPAWN_RATE=50;
int particle_count=0;
int FreeIndex=0;

typedef struct tagmyPoint
{
    int xPosition;
    int yPosition;
    int xVelocity;
    int yVelocity;
    int dead;  //by default when myPoint spawns it is not dead  
    int color;
}TJ_POINT,*PTJ_POINT,**PPTJ_POINT;

typedef struct tagVector
{
    PPTJ_POINT list;
    //tagmyPoint ** list;
	int count;
}VECTOR,*PVECTOR;

PVECTOR CreateVector(void);
int PushElement(PVECTOR pVector,PTJ_POINT pPoint);
void DestroyVector(PVECTOR pVector);
void Advance(PTJ_POINT pPoint);
void Accelerate(PTJ_POINT pPoint,int xAccel,int yAccel);
void SpawnLoop(PVECTOR pVector);
void PopElement(PVECTOR pVector,int index);

//int IsDead(PTJ_POINT pPoint);

PVECTOR PointVector;


PVECTOR CreateVector(void)
{
    PVECTOR pVector=NULL;
    pVector=(PVECTOR)malloc(sizeof(VECTOR));
    pVector->list=(PPTJ_POINT)NULL;
    pVector->count=0;
    return pVector;
}

void DestroyVector(PVECTOR pVector)
{
    int i=0;
    for (i=0;i<pVector->count;i++)
    {
        free(pVector->list[i]);
		pVector->list[i]=NULL;
    }
    free(pVector->list);
    pVector->list=NULL;
    pVector->count=0;
    free(pVector);
    pVector=NULL;
}

int PushElement(PVECTOR pVector,PTJ_POINT pPoint)
{
    pVector->count+=1;
    pVector->list=(PPTJ_POINT)realloc(pVector->list,pVector->count*sizeof(PTJ_POINT));
	
    if(pVector->list[FreeIndex]==NULL)
    {
        pVector->list[FreeIndex]=pPoint;
    }
    else
    {
        pVector->list[pVector->count-1]=pPoint;
    }
    return (EXIT_SUCCESS);
}


void PopElement(PVECTOR pVector,int index)
{
    if(index >=0 && index < pVector->count)
    {
		free(pVector->list[index]);
        pVector->list[index]=NULL;
        FreeIndex=index;       
    }
}


PTJ_POINT CreatePoint(int xPos,int yPos, int xVel, int yVel)
{
    PTJ_POINT pPoint=NULL;
	pPoint=(PTJ_POINT)malloc(sizeof(TJ_POINT));
    pPoint->xPosition=xPos;
    pPoint->yPosition=yPos;
    pPoint->xVelocity=xVel;
    pPoint->yVelocity=yVel;
	pPoint->dead=0;

    return pPoint;
}


void Accelerate(PTJ_POINT pPoint,int xAccel,int yAccel)
{
    pPoint->xVelocity+=xAccel;
    pPoint->yVelocity+=yAccel;
}


void Advance(PTJ_POINT pPoint){
    pPoint->xPosition+=pPoint->xVelocity;
    pPoint->yPosition+=pPoint->yVelocity;

    if (pPoint->xPosition<0 || pPoint->xPosition>CLIENT_XArea || pPoint->yPosition<0 || pPoint->yPosition>CLIENT_YArea )
        pPoint->dead=1;
}


void SpawnLoop(PVECTOR pVector)
{
    int i=0;
    int xVel;
    int yVel;
    PTJ_POINT pPoint;
    for(i=0;i<SPAWN_RATE;i++)
    {
        xVel=rand()%10 + 5;
        yVel=rand()%10 + 5;
        pPoint=CreatePoint(CLIENT_XArea/2,CLIENT_YArea/2,xVel,yVel);   //All points will spawn at fixed point i'e center
        PushElement(pVector,pPoint);
        particle_count+=1;  //This particle_count  is a global var which act as a Counter for total active particle which were spawned
    }

}

void AdvanceLoop(PVECTOR pVector)
{
    int i=0;
    int xAccel;
    int yAccel;
    PTJ_POINT pPoint;
    for (i=0;i<pVector->count;i++)
    {
        xAccel=rand()%10;
        yAccel=rand()%10;
        
		xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
	
		
		pPoint = pVector->list[i];
        Advance(pPoint);
		
		
        Accelerate(pPoint,xAccel,yAccel);
        if(pPoint->dead==1)
        {
             PopElement(pVector,i);
            particle_count-=1;
        }
            
    }
} 


void main()
{

     PointVector=CreateVector();  //Create Point Vector
	 int pointIndex;
	while(1)
	{
		SpawnLoop(PointVector);
            AdvanceLoop(PointVector);
            for(pointIndex=0; pointIndex < PointVector->count; pointIndex++)
            {
                if((PointVector->list[pointIndex])==NULL)
                    continue;
				
				printf("Total Points: %d CurrentPoint %d : (X,Y)=(%d,%d)\n",particle_count,pointIndex,PointVector->list[pointIndex]->xPosition,PointVector->list[pointIndex]->yPosition);
				
                //MyCircle( PointVector->list[pointIndex]->xPosition, PointVector->list[pointIndex]->yPosition,3, hdc);
				Sleep(33);
			}
	}

    //DestroyVector(PointVector); //Destroy Point Vector
}





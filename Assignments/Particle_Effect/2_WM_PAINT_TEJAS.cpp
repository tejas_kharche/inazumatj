// 1.
#include <windows.h>
#include <stdio.h>


// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

#define TJ_POINT_RADIUS 2

int CLIENT_XArea,CLIENT_YArea;
int CLIENT_XMouse,CLIENT_YMouse;
int SPAWN_RATE=7;
int particle_count=0;

typedef struct tagmyPoint
{
    int xPosition;
    int yPosition;
    int xVelocity;
    int yVelocity;
    //int xAcceleration;
    //int yAcceleration;
    int dead;  //by default when myPoint spawns it is not dead  
    int color;
}TJ_POINT,*PTJ_POINT,**PPTJ_POINT;

typedef struct tagVector
{
    PPTJ_POINT list;
    //tagmyPoint ** list;
	int count;
}VECTOR,*PVECTOR;

PVECTOR CreateVector(void);
int PushElement(PVECTOR pVector,PTJ_POINT pPoint);
void DestroyVector(PVECTOR pVector);
void Advance(PTJ_POINT pPoint);
void Accelerate(PTJ_POINT pPoint,int xAccel,int yAccel);
void SpawnLoop(PVECTOR pVector);
void PopElement(PVECTOR pVector,int index);

//int IsDead(PTJ_POINT pPoint);

PVECTOR PointVector;


PVECTOR CreateVector(void)
{
    PVECTOR pVector=NULL;
    pVector=(PVECTOR)malloc(sizeof(VECTOR));
    pVector->list=NULL;
    pVector->count=0;
    return pVector;
}

void DestroyVector(PVECTOR pVector)
{
    int i=0;
    for (i=0;i<pVector->count;i++)
    {
        free(pVector->list[i]);
		pVector->list[i]=NULL;
    }
    free(pVector->list);
    pVector->list=NULL;
    pVector->count=0;
    free(pVector);
    pVector=NULL;
}



int PushElement(PVECTOR pVector,PTJ_POINT pPoint)
{
    pVector->count+=1;
    pVector->list=(PPTJ_POINT)realloc(pVector->list,pVector->count*sizeof(PTJ_POINT));
	
    pVector->list[pVector->count-1]=pPoint;
	
    return (EXIT_SUCCESS);
}


void PopElement(PVECTOR pVector,int index)
{
    if(index <0 || index >= pVector->count)
		return;
	
	free(pVector->list[index]);
	pVector->list[index] = NULL;
	
	int i;
	for(i=index;i< pVector->count-1 ;i++)
	{
		pVector->list[i]=pVector->list[i+1];
		pVector->list[i+1] = NULL;
	}
	
	pVector->count-=1;
}


PTJ_POINT CreatePoint(int xPos,int yPos, int xVel, int yVel)
{
    PTJ_POINT pPoint=NULL;
	pPoint=(PTJ_POINT)malloc(sizeof(TJ_POINT));
    pPoint->xPosition=xPos;
    pPoint->yPosition=yPos;
    pPoint->xVelocity=xVel;
    pPoint->yVelocity=yVel;
	pPoint->dead=0;

    return pPoint;
}


void Accelerate(PTJ_POINT pPoint,int xAcc,int yAcc)
{
    pPoint->xVelocity+=xAcc;
    pPoint->yVelocity+=yAcc;
}


void Advance(PTJ_POINT pPoint){
    pPoint->xPosition+=pPoint->xVelocity;
    pPoint->yPosition+=pPoint->yVelocity;

	if(pPoint->xPosition<=0 || pPoint->xPosition>=CLIENT_XArea || pPoint->yPosition<=0 || pPoint->yPosition>=CLIENT_YArea)
	{
		pPoint->dead=1;	
	}
}	


void SpawnLoop(PVECTOR pVector)
{
    int i=0;
    int xVel;
    int yVel;
	int xAccel;
    int yAccel;
    PTJ_POINT pPoint;
    for(i=0;i<SPAWN_RATE;i++)
    {
        xVel=rand()%5;
        yVel=rand()%5;  
		
		xAccel=rand()%3;
        yAccel=rand()%3;
		
		xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
    
	
        pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel);   //point will be created at current mouse position
        //pPoint=CreatePoint(CLIENT_XArea,CLIENT_YArea,xVel,yVel);  //All points will start from screen's center 
        PushElement(pVector,pPoint);
        particle_count+=1;  //This particle_count  is a global var which act as a Counter for total active particle which were spawned
    }
	pVector=NULL;
}


void AdvanceLoop(PVECTOR pVector)
{
    int i=0;
    int xAccel;
    int yAccel;
    //PTJ_POINT pPoint;
	
    for (i=0;i<pVector->count;i++)
    {
        //if(PointVector->list[i]==NULL)
		//continue;
        xAccel=rand()%3;
        yAccel=rand()%3;
		
		xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
    
		Advance(pVector->list[i]);
		
		Accelerate(pVector->list[i],xAccel,yAccel);
		
        if(pVector->list[i]->dead==1)
        {
            PopElement(pVector,i);
            particle_count-=1;
        }

    } 
}

//2. Hollywood Principal
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

///MY Circle

bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc);



//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{

    //My Addon

     PointVector=CreateVector();  //Create Point Vector

	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC"); // name of class 
    TCHAR szWindowCaption[] = TEXT("104_Tejas : BASIC PARTICLE SYSTEM");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_QUESTION); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	wndclass.lpszClassName = szAppName; // Group Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// 1st 
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// 10
    DestroyVector(PointVector); //Destroy Point Vector
	PointVector=NULL;
	return((int)msg.wParam);
}


// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b

    
    static int cxClient, cyClient, cxChar, cyChar, cxMouse,cyMouse;
    HDC hdc = NULL; 

    TEXTMETRIC tm; 
    PAINTSTRUCT ps; 
	int xAccel;
    int yAccel;
	int pointIndex;

    switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		MessageBox(NULL, TEXT("Window Created!"), TEXT("1st WND"), MB_OK);
	       	SetTimer(hwnd, 1,17, NULL);
            hdc = GetDC(hwnd); 
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; 
            cyChar = tm.tmHeight + tm.tmExternalLeading; 
            ReleaseDC(hwnd, hdc); 
            hdc = NULL; 
    	break;

        case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
            CLIENT_XArea = cxClient;
            CLIENT_YArea = cyClient;
            break; 


        case WM_MOUSEMOVE:
            cxMouse = LOWORD(lParam);
            cyMouse = HIWORD(lParam);
			CLIENT_XMouse=cxMouse;
			CLIENT_YMouse=cyMouse;
            break;

        case WM_PAINT: 
            hdc = BeginPaint(hwnd, &ps); 
            
			
			//Render loop
			for(pointIndex=0; pointIndex < PointVector->count; pointIndex++)
            {
				MyCircle( PointVector->list[pointIndex]->xPosition, PointVector->list[pointIndex]->yPosition, TJ_POINT_RADIUS , hdc);	
			}
			
			EndPaint(hwnd, &ps); 
            hdc = NULL; 
            break;


     case WM_TIMER:
    {
        SpawnLoop(PointVector);	
		AdvanceLoop(PointVector);
        InvalidateRect(hwnd, NULL, TRUE);
        return 0;
    }


	case WM_DESTROY:
		MessageBox(NULL, TEXT("Window Destroyed"), TEXT("1st WND Going"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc)
{
    return(Ellipse(hdc,(iXCenter-radius),(iYCenter-radius),(iXCenter+radius),(iYCenter+radius)));
}
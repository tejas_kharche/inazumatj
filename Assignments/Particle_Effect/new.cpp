#include <Windows.h>
#include <windowsx.h>
#include <tchar.h>
#include <iostream>


TCHAR szWindowClass[] = TEXT("CreateThreadWindow");
TCHAR szAppName[] = TEXT("CreateThreadExample");

BOOL InitWindow(HINSTANCE, int);
ATOM MyRegisterClass(HINSTANCE);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

HWND parentWindow;
MSG msg;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) 
{
    MyRegisterClass(hInstance);
    if (!InitWindow(hInstance, nCmdShow))
        return FALSE;
    BOOL bRet;
    while ((bRet = GetMessage(&msg, (HWND)NULL, 0, 0)) != 0)
    {
        if (bRet == -1)
            return FALSE;
        else
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return (int)msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASS wndClass;
    memset(&wndClass, 0, sizeof(wndClass));
    wndClass.lpfnWndProc = WndProc;
    wndClass.hInstance = hInstance;
    wndClass.lpszMenuName = NULL;
    wndClass.lpszClassName = szWindowClass;
    return RegisterClass(&wndClass);
}

BOOL InitWindow(HINSTANCE hInstance, int nCmdShow)
{
    parentWindow = CreateWindow(szWindowClass, szAppName, WS_OVERLAPPEDWINDOW,
        300, 0, 600, 600, NULL, NULL, hInstance, NULL);
    ShowWindow(parentWindow, nCmdShow);
    return TRUE;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    static int x = 0, y = 0, width = 200, height = 100;
    switch (iMsg) {
    case WM_ERASEBKGND:
    {
        _RPT1(0, "%s\n", "erase");
        break;
    }
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hWnd, &ps);
        Ellipse(hdc, x, y, width, height);
        EndPaint(hWnd, &ps);
        return 0;
    }
    case WM_CREATE:
    {
        SetTimer(hWnd, 1, 1000, NULL);
        break;
    }
    case WM_TIMER:
    {
        x += 5;
        InvalidateRect(hWnd, NULL, TRUE);
        return 0;
    }
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    default:
        break;
    }

    return DefWindowProc(hWnd, iMsg, wParam, lParam);
}

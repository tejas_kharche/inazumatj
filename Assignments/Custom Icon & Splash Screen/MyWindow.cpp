/*
   This Program demonstrates how to load a bitmap as a resource, how to set into in memory device context and transferring it to Physical device's device context.
   This program shows that all gdi functions are very much similar in their working. 
	   0. Load a GDI Object and get it's HANDLE in WM_CREATE
	   1. Create/Get a suitable Device Context (DC), for bitmat it is a Compatible DC  i.e. in memory DC.
	   2. Set/Select it into DC.
	   3. Get GDI Object's information or size/dimension in bitmap case.
	   4. Draw or Show -> Here in this case moving data fron in memory device context to physical device context. (BitBlt i.e. Bit Block Transfer)
	   5. Delete/Free the device context after its use. Because it is a heavy resource.
	   6. Unload a resource in WM_DESTROY
   
   Resoure should be drawn or shown or visible on your window.
   
   Resource Compilation and Build steps:
   rc.exe MyResource.rc -> MyResource.res // binary resource file
   cl.exe /c /EHsc MyWindow.cpp -> MyWindow.obj
   link.exe MyWindow.obj MyResource.res user32.lib gdi32.lib /SUBSYSTEM:WINDOWS -> MyWindow.exe
   
   Note: Bitmap becomes part of your executable. Hence this increases size of our executable.
   
 */

#include<windows.h>
#include"MyResource.h"

#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")


// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	//wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	 wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	 wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	//wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("104_TEJAS's Splash Screen Demo"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	HDC hdc;
	HDC hdcComp;
	PAINTSTRUCT ps;
	HINSTANCE hInst;
	static HBITMAP hbmap;
	HGDIOBJ prevHGDIObj = NULL;
	BITMAP bmBuffer;
	RECT rc;
	// code
	switch (iMsg) 
	{
	case WM_CREATE:
		hInst = (HINSTANCE)((LPCREATESTRUCT)lParam)->hInstance;
		hbmap = LoadBitmap(hInst, MAKEINTRESOURCE(MY_BITMAP));
		break;
	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		hdcComp = CreateCompatibleDC(hdc);
		prevHGDIObj = SelectObject(hdcComp, hbmap);
		GetObject(hbmap, sizeof(BITMAP), &bmBuffer); // retrieves dimension of a bitmap
		GetClientRect(hwnd, &rc);  // fill the x,y, width, height into rectangle
		BitBlt(hdc, (rc.right - bmBuffer.bmWidth)/2, (rc.bottom - bmBuffer.bmHeight)/2, (int)bmBuffer.bmWidth , (int)bmBuffer.bmHeight, hdcComp, 0, 0, SRCCOPY); // performs bit block transfer from memory DC to hdc returned from BeginPaint
		SelectObject(hdcComp, prevHGDIObj);
		DeleteDC(hdcComp);
		EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		DeleteObject(hbmap);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
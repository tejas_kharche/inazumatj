// 1.
#include <windows.h>

// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")


#define _USE_MATH_DEFINES
#include <math.h>

//2. Hollywood Principal
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
COLORREF * getRandomColor();

//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{
	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC"); // name of class 
    TCHAR szWindowCaption[] = TEXT("104_Tejas : WM_PAINT");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_QUESTION); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);

	wndclass.lpszClassName = szAppName; // Group Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// 1st 
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// 10
	return((int)msg.wParam);
}

// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b
    static int cxClient, cyClient, cxChar, cyChar; 
    static TCHAR* ppszGroupName[] = {TEXT("Dennis Ritchie Group"), 
                                     TEXT("Ken Thompson Group"), 
                                     TEXT("Donald Knuth Group"), 
                                     TEXT("Helen Cluster Group"), 
                                     TEXT("Linus Torvalds Group"),
                                     TEXT("Lady Ada lovelace Group"), 
                                     TEXT("David Cutler Group"),
                                     TEXT("Jon Von Nueman Group"), 
                                     TEXT("Alan Turing Group"),
                                     TEXT("James Gosling Group"),
                                     TEXT("Guido Rossum Group"),
                                     TEXT("Charles Petzold Group"),
									 TEXT("Tejas")};                                 
    HDC hdc = NULL; 

    TEXTMETRIC tm; 
    PAINTSTRUCT ps; 
	int Offset=15;
	//COLORREF * p_color;
	switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		MessageBox(NULL, TEXT("Window Created!"), TEXT("1st WND"), MB_OK);
	       	
            hdc = GetDC(hwnd); 
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; 
            cyChar = tm.tmHeight + tm.tmExternalLeading; 
            ReleaseDC(hwnd, hdc); 
            hdc = NULL; 
    	break;

        case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
            break; 

        case WM_PAINT: 
            hdc = BeginPaint(hwnd, &ps); 
           //1 st quadarent
			
            SetTextAlign(hdc, TA_LEFT | TA_TOP); 
			SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor())); 
			
            TextOut(hdc, 0, 0, ppszGroupName[0], lstrlen(ppszGroupName[0])); 
            
			SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
			SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			
            TextOut(hdc, cxClient/4, cyClient/4, ppszGroupName[1], lstrlen(ppszGroupName[1])); 
            
			SetTextAlign(hdc, TA_RIGHT | TA_BOTTOM); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			
			TextOut(hdc, cxClient/2-Offset, cyClient/2-Offset, ppszGroupName[2], lstrlen(ppszGroupName[2])); 
           
           //2 nd quadarent
            SetTextAlign(hdc, TA_RIGHT | TA_TOP); 
			SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
            
			TextOut(hdc, cxClient, 0, ppszGroupName[3], lstrlen(ppszGroupName[3])); 
            SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
			SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			
            TextOut(hdc, 3*cxClient/4, cyClient/4, ppszGroupName[4], lstrlen(ppszGroupName[4])); 
            SetTextAlign(hdc, TA_LEFT | TA_BOTTOM); 
            TextOut(hdc, cxClient/2+Offset, cyClient/2-Offset, ppszGroupName[5], lstrlen(ppszGroupName[5])); 
           
           //3rd Row quadarent
            SetTextAlign(hdc, TA_RIGHT | TA_TOP); 
			SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc,cxClient/2-Offset, cyClient/2+Offset, ppszGroupName[6], lstrlen(ppszGroupName[6])); 
            
			SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc, cxClient/4, 3*cyClient/4, ppszGroupName[7], lstrlen(ppszGroupName[7])); 
            
			SetTextAlign(hdc, TA_LEFT | TA_BOTTOM); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc, 0, cyClient, ppszGroupName[8], lstrlen(ppszGroupName[8]));
           
			//4th quadarent
            SetTextAlign(hdc, TA_LEFT | TA_TOP); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc,cxClient/2+Offset , cyClient/2+Offset, ppszGroupName[9], lstrlen(ppszGroupName[9])); 
            
			SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc, 3*cxClient/4, 3*cyClient/4, ppszGroupName[10], lstrlen(ppszGroupName[10])); 
			
			SetTextAlign(hdc, TA_RIGHT | TA_BOTTOM); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc, cxClient, cyClient, ppszGroupName[11], lstrlen(ppszGroupName[11]));
           
			SetTextAlign(hdc, TA_CENTER | TA_CENTER ); 
            SetBkMode(hdc,TRANSPARENT);
			SetTextColor(hdc,*(getRandomColor()));
			TextOut(hdc, cxClient/2, cyClient/2-(cyChar/2), ppszGroupName[12], lstrlen(ppszGroupName[12]));

            EndPaint(hwnd, &ps); 
            hdc = NULL; 
            break;

		// Last msg
	case WM_DESTROY:
		MessageBox(NULL, TEXT("Window Destroyed"), TEXT("1st WND Going"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


COLORREF * getRandomColor()
{
	int choice;
	COLORREF * color;
	choice=rand()%9;
	
	
	COLORREF red=RGB(255,0,0); 
    COLORREF green=RGB(0,255,0); 
    COLORREF blue=RGB(0,0,255); 
	COLORREF orange=RGB(255,165,0); 
    COLORREF cyan=RGB(0,255,255); 
    COLORREF magenta=RGB(255,0,255); 
	COLORREF yellow=RGB(255,215,0); 
	COLORREF skyblue=RGB(0,191,255); 
	
	switch(choice)
	{
		case 1 :  
			color=&red;
			break;
		case 2 :
			color=&green;
			break;
		case 3 :
			color=&blue;
			break;
		case 4 :
			color=&orange;
			break;
		case 5 :
			color=&cyan;
			break;
		case 6 :
			color=&magenta;
			break;
		case 7:
			color=&yellow;
			break;
		case 8 :
			color=&skyblue;
			break;
		default:
			color=&orange;
	}
	
	return color;
}
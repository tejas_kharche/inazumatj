#include <stdio.h>
#include <stdlib.h>


#define FAILURE 0
#define SUCCESS 1
#define LIST_EMPTY 2
#define DATA_NOT_FOUND 3

#define assert(CONDITION)  do{\
						    if(!(CONDITION))\
						    	exit(EXIT_FAILURE);\
				        }while(0)

typedef enum{FALSE=0,TRUE} bool;
typedef int status_t;
typedef int len_t;
typedef int count_t;
typedef int data_t;
typedef struct node
{
	int data;
	struct node * next;
} node_t,list_t;

//auxilary functions

node_t * get_list_node(data_t in_data);

void * xmalloc(size_t size_in_bytes);

status_t generic_insert(node_t *p_before,node_t *p_mid,node_t * p_after);

status_t generic_delete(list_t * p_list,node_t * p_delete_node);

node_t * locate_node(list_t * p_list,data_t e_data);

//interface functions

void display_list(list_t * p_list,char * message);

//insert
status_t insert_beg(list_t * p_list, data_t in_data);
status_t insert_end(list_t * p_list, data_t in_data);
status_t insert_before(list_t * p_list, data_t e_data,data_t in_data);
status_t insert_after(list_t * p_list,data_t e_data, data_t in_data);


//Remove

status_t remove_beg(list_t * p_list);
status_t remove_end(list_t * p_list);
status_t remove_data(list_t * p_list,data_t e_data);


//get & pop
status_t get_beg(list_t * p_list,data_t * p_o_data);
status_t get_end(list_t * p_list,data_t * p_o_data);
status_t pop_beg(list_t * p_list,data_t * p_o_data);
status_t pop_end(list_t * p_list,data_t * p_o_data);


bool list_contains(list_t * p_list, data_t e_data);

count_t get_count(list_t *p_list, data_t e_data);

len_t get_length(list_t * p_list);

// cteate and distroy

list_t* create_list();
status_t destroy_list(list_t * p_list);

int main(int argc, char *argv[])
{
   status_t status;
   data_t data;
   list_t * p_list1=create_list();
   list_t * p_list2=create_list();
   list_t * p_list3=create_list();
   
    insert_beg(p_list1,10);
	insert_beg(p_list1,20);
	insert_beg(p_list1,30);
	insert_beg(p_list1,40);
	insert_beg(p_list1,50);
	
	display_list(p_list1,"list 1 : Insert_beg()");
	
	insert_end(p_list1,60);
	insert_end(p_list1,70);
	insert_end(p_list1,80);
	
	display_list(p_list1,"list 1 : Insert_end()");
	
	insert_before(p_list1,80,100);
	insert_before(p_list1,50,678);
	insert_before(p_list1,70,55);
	
	display_list(p_list1,"list 1 : Insert_before()");
	
	insert_after(p_list1,80,777);
	insert_after(p_list1,50,888);
	insert_after(p_list1,70,999);
	
	display_list(p_list1,"list 1 : Insert_after()\n");
	puts("");
	display_list(p_list1,"list 1 : Before remove_beg()\n");
	status=remove_beg(p_list1);
	printf("\nStatus : %d \n",status);
	display_list(p_list1,"\nlist 1 : After remove_beg()\n");
	puts("");
display_list(p_list1,"\nlist 1 : Before remove_end()\n");
	status=remove_end(p_list1);
	printf("\nStatus : %d \n",status);
	display_list(p_list1,"\nlist 1 : After remove_end()\n");

	
	status=remove_data(p_list1,70);
	printf("\nStatus : %d \n",status);
	display_list(p_list1,"list 1 : remove_data(70)\n");
	
	status=remove_data(p_list1,567);
	printf("\nStatus : %d \n",status);
	display_list(p_list1,"list 1 : remove_data(567)\n");
	
	status=remove_data(p_list2,567);
	printf("\nStatus : %d \n",status);
	display_list(p_list2,"list 2 : remove_data(567)\n");
	
	//get pop
	
	display_list(p_list1,"\nlist 1 : BEFORE get_beg()\n");
	status=get_beg(p_list1,&data);
	printf("\nGET BEG  : %d \n",data);
	printf("\nStatus : %d \n",status);
	puts("");
	display_list(p_list1,"\nlist 1 : AFTER get_beg()\n");
	
	
	display_list(p_list1,"\nlist 1 : BEFORE get_end()\n");
	status=get_end(p_list1,&data);
	printf("\nGET END : %d \n",data);
	printf("\nStatus : %d \n",status);
	puts("");
	display_list(p_list1,"\nlist 1 : AFTER get_end()\n");
	
	display_list(p_list1,"\nlist 1 : BEFORE pop_beg()\n");
	status=pop_beg(p_list1,&data);
	printf("\nPOP BEG  : %d \n",data);
	printf("\nStatus : %d \n",status);
	puts("");
	display_list(p_list1,"\nlist 1 : AFTER pop_beg()\n");
	
	
	display_list(p_list1,"\nlist 1 : BEFORE pop_end()\n");
	status=pop_end(p_list1,&data);
	printf("\nPOP END : %d \n",data);
	printf("\nStatus : %d \n",status);
	puts("");
	display_list(p_list1,"\nlist 1 : AFTER pop_end()\n");
	
	
	
	status=get_beg(p_list2,&data);
	printf("\n get_beg() Status : %d \n",status);
	status=get_end(p_list2,&data);
	printf("\n get_end() Status : %d \n",status);
	status=pop_beg(p_list3,&data);
	printf("\n pop_beg() Status : %d \n",status);
	status=pop_end(p_list3,&data);
	printf("\n pop_end() Status : %d \n",status);
	

}

void * xmalloc(size_t size_in_bytes)
{
	void * p=NULL;
	p=malloc(size_in_bytes);
	assert(p!=NULL);
	return(p);
}

list_t* create_list()
{
	list_t *p=NULL;
	p=(list_t*)xmalloc(sizeof(list_t));
	p->data=0;
	p->next=p;
	
	return(p);
}

status_t destroy_list(list_t * p_list)
{
	node_t * p_run=p_list->next;
	node_t * p_run_next=NULL;
	
	while( p_run != p_list)
	{
         p_run_next=p_run->next;
         generic_delete(p_list,p_run);
         p_run=p_run_next;
	}
	return(SUCCESS);
}

node_t * get_list_node(data_t in_data)
{
	node_t * p=NULL;
	p=(node_t *)xmalloc(sizeof(node_t));
	p->data=in_data;
	p->next=NULL;
	
	return (p);
}

node_t * locate_node(list_t * p_list,data_t e_data)
{
	node_t * p_run=p_list->next;
	
	while(p_run != p_list)
	{
		if(p_run->data == e_data){
			return(p_run);
		}
		p_run=p_run->next;
	}
	
	return(NULL);
}

status_t generic_insert(node_t *p_before,node_t *p_mid,node_t * p_after)
{
	p_before->next=p_mid;
	p_mid->next=p_after;
	return(SUCCESS);
}

status_t generic_delete(list_t * p_list,node_t * p_delete_node)
{
	node_t * p_run = p_list->next;
	node_t * p_run_prev= p_list;
	node_t * p_run_next=p_delete_node->next;
	
	//since in singly linked list we dint have previous node link we have to trvarese the liat to get previous node
	
	while (p_run != p_delete_node)
	{
		p_run_prev=p_run;
		p_run=p_run->next;
	}
	
	p_run_prev->next=p_run_next;
	return(SUCCESS);
}

void display_list(list_t * p_list,char * message)
{
	if(message != NULL)
		printf("%s\n",message);
		
	node_t * p_run=p_list->next;
	
	printf("[Start]->");
	while(p_run != p_list)
	{
		printf("[%d]->",p_run->data);
		p_run=p_run->next;
	}
	printf("[End]");
}


status_t insert_beg(list_t * p_list, data_t in_data)
{
	generic_insert(p_list,get_list_node(in_data),p_list->next);
	return(SUCCESS);
}

status_t insert_end(list_t * p_list, data_t in_data)
{
	
	node_t *p_run=p_list->next;
	
	//trave p_run to the end of list to get the last node
	while(p_run->next != p_list)
	{
		p_run=p_run->next;
	}
	
	generic_insert(p_run,get_list_node(in_data),p_list);
	
	return(SUCCESS);
	
}


status_t insert_before(list_t * p_list,data_t e_data, data_t in_data)
{
	
	node_t* p_run=p_list->next;
	if( p_list->next == p_list){
		return(LIST_EMPTY);
	}
	
	node_t * p=locate_node(p_list,e_data);
	if(p==NULL)
	{
		return(DATA_NOT_FOUND);
	}
	
	//traverse list till node whose next is desired node
	while(p_run->next != p)
	{
		p_run=p_run->next;
	}
	
	generic_insert(p_run,get_list_node(in_data),p);
	
	
	return(SUCCESS);
	
}

status_t insert_after(list_t * p_list,data_t e_data, data_t in_data)
{
	
	if( p_list->next == p_list){
		return(LIST_EMPTY);
	}
	
	node_t * p=locate_node(p_list,e_data);
	if(p==NULL)
	{
		return(DATA_NOT_FOUND);
	}
	
	//traverse list till node whose next is desired node

	generic_insert(p,get_list_node(in_data),p->next);
	
	
	return(SUCCESS);
	
}

status_t remove_beg(list_t * p_list)
{
	if(p_list->next == p_list)
		return(LIST_EMPTY) ;
		
	generic_delete(p_list,p_list->next);
	return(SUCCESS);
}

status_t remove_end(list_t * p_list)
{
	node_t * p_run=p_list->next;
	
	if(p_list->next == p_list)
		return(LIST_EMPTY) ;
		
	//Traverse to last node
	while(p_run->next != p_list)
	{
		p_run=p_run->next;
	}
	
	generic_delete(p_list,p_run);
	
	return(SUCCESS);
}

status_t remove_data(list_t * p_list,data_t e_data)
{
	node_t * p=locate_node(p_list,e_data);
	
	if(p_list->next == p_list)
	{
		return(LIST_EMPTY);
	}
	
	if(p==NULL)
	{
		return(DATA_NOT_FOUND);
	}
	
	generic_delete(p_list,p);
	
	return(SUCCESS);
}


status_t get_beg(list_t * p_list,data_t * p_o_data)
{
	if( p_list->next == p_list)
	{
		return (LIST_EMPTY);
	}
	
	*p_o_data=p_list->next->data;
	return(SUCCESS) ;
}

status_t get_end(list_t * p_list,data_t * p_o_data)
{
	if( p_list->next == p_list)
	{
		return (LIST_EMPTY);
	}
	
	node_t * p_run=p_list->next;
	node_t * p_end=p_list;
	while(p_run!=p_list)
	{
		p_end=p_run;
		p_run=p_run->next;
	}
	
	*p_o_data=p_end->data;
	
	return(SUCCESS) ;
}

status_t pop_beg(list_t * p_list,data_t * p_o_data)
{
	if( p_list->next == p_list)
	{
		return (LIST_EMPTY);
	}
	
	*p_o_data=p_list->next->data;
	generic_delete(p_list,p_list->next);
	
	return(SUCCESS) ;
}

status_t pop_end(list_t * p_list,data_t * p_o_data)
{
    if( p_list->next == p_list)
	{
		return (LIST_EMPTY);
	}
    node_t * p_run=p_list->next;
	node_t * p_end=p_list;
	while(p_run!=p_list)
	{
		p_end=p_run;
		p_run=p_run->next;
	}
	*p_o_data=p_end->data;
	generic_delete(p_list,p_end);
	return(SUCCESS) ;
}

bool list_contains(list_t * p_list, data_t e_data)
{
	node_t *p=locate_node(p_list,e_data);
	if(p==NULL)
		return(FALSE);
	else
		return(TRUE) ;
}


count_t get_count(list_t *p_list, data_t e_data)
{
	int count=0;
	node_t * p_run=p_list->next;
	
	while(p_run!=p_list)
	{
		if(p_run->data==e_data)
			count++;
		p_run=p_run->next;
	}
	return(count);
}

len_t get_length(list_t * p_list)
{
	int list_length=0;
	node_t * p_run=p_list->next;
	
	while(p_run!=p_list)
	{
		p_run=p_run->next;
	}
	return(list_length);
}

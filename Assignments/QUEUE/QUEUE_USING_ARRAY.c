//######### ############# 
//	Author : Tejas Kharche        #
//###################### 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SUCCESS 1
#define FAILURE 0
#define QUEUE_EMPTY 2
#define assert(CONDITION) do{\
													if(!(CONDITION))\
														exit(1);\
													}while(0);


typedef int status_t;
typedef int data_t;
typedef enum{FALSE=0,TRUE} bool;

typedef struct queue
{
		data_t * Array;
		size_t ArraySize;
		int TopIndex;
} queue_t;


queue_t * createQueue(size_t init_size);

status_t add(queue_t * p_queue, data_t in_data);
status_t delete(queue_t * p_queue, data_t * out_data);
status_t peak(queue_t * p_queue, data_t * out_data);
void displayQueue(queue_t * p_queue,char * message);

bool isQueueEmpty(queue_t * p_queue);
void * xmalloc(size_t size_in_bytes);

status_t destroyQueue(queue_t * p_queue);


int main(int argc, char *argv[])
{
	int i=0;
	status_t status;
	data_t out_data;
	queue_t * p_queue=createQueue(8);
	
	for(i=0;i<5;i++)
	{
		add(p_queue,(i+1)*10);
		printf("\nDisplay Queue After Adding %d\n",(i+1)*10);
		displayQueue(p_queue,NULL);
	}
	
	
	status=peak(p_queue,&out_data);
	printf("Peak() Status : %d  |  Top :  %d | Item : %d \n",status,p_queue->TopIndex, out_data);
	
	
	while(!isQueueEmpty(p_queue))
	{
		status=delete(p_queue,&out_data);
		printf("DELETE() Status : %d  |  Top :  %d | Item : %d \n",status,p_queue->TopIndex, out_data);
		printf("\nDisplay Queue Removing %d\n",out_data);
		displayQueue(p_queue,NULL);
	}
	
	destroyQueue(p_queue);
 
}

queue_t * createQueue(size_t init_size)
{
	queue_t * p_queue=NULL;		
	p_queue=(queue_t *)xmalloc(sizeof(queue_t));
	p_queue->Array=(data_t  *)xmalloc( init_size*sizeof(data_t) );
	memset(p_queue->Array, 0 ,init_size*sizeof(data_t) );
	p_queue->ArraySize=init_size;
	//p_queue->TopIndex=0;
	p_queue->TopIndex=-1;
	
	return (p_queue);	
}

status_t add(queue_t * p_queue, data_t in_data)
{
	//if( p_queue->ArraySize == p_queue->TopIndex)
	if( p_queue->ArraySize-1 == p_queue->TopIndex)
	{
		p_queue->ArraySize++;
		p_queue->TopIndex++;
		puts("Queue OUT OF predefined Queue size");	
		p_queue->Array=(data_t *)realloc(p_queue->Array,p_queue->ArraySize*sizeof(data_t));
		assert(p_queue->Array != NULL);
		
		p_queue->Array[p_queue->TopIndex]=in_data;
								
	}//else if ( p_queue->TopIndex < p_queue->ArraySize)
	else
	{
		puts("Queue WITHIN predefined Queue size");	
		p_queue->TopIndex++;
		p_queue->Array[p_queue->TopIndex]=in_data;
	}	
	
}

status_t delete(queue_t * p_queue, data_t * out_data)
{
	data_t * p_old_array=NULL;
	data_t * p_new_array=NULL;
		
	if(isQueueEmpty(p_queue)){
		return(QUEUE_EMPTY); 	
	}
	else{	
	*out_data=p_queue->Array[0];
	p_queue->ArraySize--;
	p_queue->TopIndex--;
	//Followig steps is extremely crucial as we are removing the element  from the first side and we  know i.e the base adess will be gone so we should cpture the next element's address in array and treat it  as base address.'
	p_old_array=p_queue->Array;
	p_new_array=p_queue->Array+1;			
	p_queue->Array=(data_t *)realloc(p_new_array, p_queue->ArraySize* sizeof(data_t));
	//assert(p_queue->Array != NULL);	
	free(p_old_array);
	p_old_array=NULL;
	p_new_array=NULL;
	return(SUCCESS);
	}
}

status_t peak(queue_t * p_queue, data_t * out_data)
{
	if(isQueueEmpty(p_queue)){
		return(QUEUE_EMPTY); 	
	}else{
		*out_data=p_queue->Array[0];
		return(SUCCESS);		
	}
				
}

bool isQueueEmpty(queue_t * p_queue)
{
	if(p_queue->TopIndex == -1){
		return(TRUE);
	}else{
		return(FALSE);
	}
}

status_t destroyQueue(queue_t * p_queue)
{
	if(p_queue != NULL)
	{
		free(p_queue->Array);
		p_queue->Array=NULL;
		free(p_queue);
		p_queue=NULL;
		return(SUCCESS);		
	}
	return (FAILURE);		
}


void * xmalloc(size_t size_in_bytes)
{
	void * p=NULL;
	p=malloc(size_in_bytes);
	assert(p!=NULL);
	return (p);
}


void displayQueue(queue_t * p_queue,char * message)
{
	if(message != NULL){
		printf("%s",message);
	}
	int i;
	puts("TOP");
	for(i=0;i<=p_queue->TopIndex;i++)
	{
		printf("[%d]\n",p_queue->Array[i]);	
	}
	puts("END");
}
	

//#############################
//      Name : Tejas										#
//	  MSTC													 #
//	  Assignment: QUEUE USING DCLL	#
//#############################

#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 									   1
#define FAILURE 										 0
#define QUEUE_EMPTY 							2
#define NULL_POINTER_EXCEPTION 	3
#define assert(CONDITION) do{\
													if(!(CONDITION))\
													{\
														puts("ASSERT MACRO:   CONDITION NOT SATISFIED");\
														exit(1);\
													}\
													}while(0)

typedef enum{FALSE=0,TRUE} bool;

typedef struct node{
	int data;
	struct node * next;
	struct node * prev;
}node_t,queue_t;

typedef int status_t;
typedef int data_t;
typedef int len_t;
 


//////// INTERNAL	FUNCTIONS	//////////

status_t genericInsert(queue_t * p_beg, queue_t * p_mid, queue_t * p_end);
status_t genericDelete(queue_t * p_node);
void * xmalloc(size_t size_in_byte);

node_t * createNode(data_t in_data);


//////// INTERFACE	FUNCTIONS	//////////
queue_t * createQueue();

bool isQueueEmpty(queue_t * p_queue);

status_t insert(queue_t * p_queue,data_t in_data);
status_t delete(queue_t * p_queue, data_t * p_out_data);
status_t get(queue_t * p_queue, data_t * p_out_data);

status_t displayQueue(queue_t * p_queue, char * message);

status_t destroyQueue(queue_t **p_queue);

int main(int argc, char *argv[])
{
	status_t status;
	data_t data;
	queue_t * p_queue=createQueue();
	int i;
	
	for(i=1 ;i<=5;i++)
	{
		insert(p_queue, i*10);
		displayQueue(p_queue,"DISPLAY Queue after insert()\n");
	}
	
	
	
	displayQueue(p_queue,"Queue Before delete()\n");
	while(!isQueueEmpty(p_queue))	
	{	
		status=delete(p_queue,&data);
		if(status== SUCCESS)
		{
			printf("REMOVE ELEMENT : %d  \n",data);
			displayQueue(p_queue,"Queue After delete()\n");
		}
		puts("#############################################################");
		status=get(p_queue,&data);
		if(status==SUCCESS)
		{
			printf(" GET ELEMENT : %d \n ",data);
			displayQueue(p_queue,"Queue After get()\n");
		}
	}
	
	destroyQueue(&p_queue);
	
	//jsut checkigng if the queue is destroyed or  not
	if(p_queue == NULL)
		printf("QUEUE DESTROYED");
	else
		printf("QUEUE NOT DESTROYED");
		
}

status_t genericInsert(queue_t * p_beg, queue_t * p_mid, queue_t * p_end)
{
	p_mid->next=p_end;
	p_mid->prev=p_beg;	
	p_beg->next=p_mid;
	p_end->prev=p_mid;
			
	return (SUCCESS);	
}

status_t genericDelete(queue_t * p_node)
{
	p_node->prev->next=p_node->next;
	p_node->next->prev=p_node->prev;
	p_node->next=NULL;
	p_node->prev=NULL;
	free(p_node);
	p_node=NULL;		
	return (SUCCESS);
}


bool isQueueEmpty(queue_t * p_queue)
{
 	if (p_queue->next == p_queue && p_queue->prev == p_queue)
 		return (TRUE);
 	else
 		return (FALSE); 
}

void * xmalloc(size_t size_in_byte)
{
	void * p=NULL;
	p=malloc(size_in_byte);
	assert(p != NULL);
	return (p);	
}

queue_t * createQueue()
{
	node_t * p_queue=NULL;
	p_queue=(queue_t *)xmalloc(sizeof(queue_t));
	p_queue->data=0;
	p_queue->next=p_queue;
	p_queue->prev=p_queue;
	
	return(p_queue);
}

node_t * createNode(data_t in_data)
{
	node_t * p=NULL;
	p=(node_t *)xmalloc(sizeof(node_t));
	p->data=in_data;
	p->prev=NULL;
	p->next=NULL;
	
	return(p);
}

status_t insert(queue_t * p_queue,data_t in_data)
{
		if(p_queue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
		}
	genericInsert(p_queue->prev,createNode(in_data),p_queue);	
	return(SUCCESS);
}


status_t delete(queue_t * p_queue, data_t * p_out_data)
{
	if(p_queue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isQueueEmpty(p_queue)){
		return(QUEUE_EMPTY);
	}
	
	*p_out_data=p_queue->next->data;
	genericDelete(p_queue->next);
	return(SUCCESS);
}
status_t get(queue_t * p_queue, data_t * p_out_data)
{
	if(p_queue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isQueueEmpty(p_queue)){
			return(QUEUE_EMPTY);
	}
	
	*p_out_data=p_queue->next->data;
	return(SUCCESS);
}

status_t destroyQueue(queue_t **p_queue)
{
	if(*p_queue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	node_t * p_run=(*p_queue)->next;
	node_t * p_run_next=NULL;
	while(p_run != *p_queue)
	{
		p_run_next=p_run->next;
		genericDelete(p_run);
		p_run=p_run_next;
	}
	
	free(*p_queue);
	*p_queue=NULL;
	
	return(SUCCESS);
	
}

status_t displayQueue(queue_t * p_queue, char * message)
{
	if(p_queue == NULL){
		return(NULL_POINTER_EXCEPTION);	
	}
	
	if(message != NULL){
		printf("%s",message);
	}
	
	node_t * p_run=p_queue->next;
	printf("[START]<->");
	while(p_run != p_queue)
	{
		printf("[%d]<->",p_run->data);
		p_run=p_run->next;
	}
	puts("[END]");
	
	return(SUCCESS);
}
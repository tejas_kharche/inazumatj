#include <stdio.h>
#include <stdlib.h>



////Symbolic constants

#define  SUCCESS                   1
#define  FAILURE                      0
#define  LIST_EMPTY              2
#define  DATA_NOT_FOUND   3




#define assert(CONDITION)  do{\
                                           if(!(CONDITION))\
                                           {\
                                           puts("CONDTION NOT SATISFIED");\
                                          exit(FAILURE);\
                                           }\
                                     }while(0)
     
                                     
                                                                                   struct  node{	
	int data;
	struct node *prev;
	struct node *next;		
};

                                     


////typedefs

typedef enum{FALSE=0,TRUE} lbool;
typedef struct node node_t;
typedef struct node list_t;
typedef int status_t;
typedef int data_t;
typedef int len_t;
typedef int count_t;

////Auxilary functions

void * xmalloc(size_t size_in_bytes);
void generic_insert(node_t *p_beg,node_t * p_mid,node_t * p_end);
void generic_delete(node_t * p_deleteNode);
node_t * get_list_node(data_t in_data);
node_t * locate_node(list_t * p_list,data_t search_data);


///Inerface Routines

list_t * create_list(void);
status_t destroy_list(list_t * p_list);


//INSERT
status_t insert_beg(list_t *p_head, data_t in_data);
status_t insert_end(list_t *p_head, data_t in_data);
status_t insert_after(list_t *p_head, data_t e_data,data_t in_data);
status_t insert_before(list_t *p_head, data_t e_data,data_t in_data);

//DISPLAY
void display_list(list_t * p_list,char * msg);


//Data access
status_t get_beg(list_t *p_head,data_t *data);
status_t get_end(list_t *p_head,data_t *data);
status_t pop_beg(list_t *p_head,data_t *data);
status_t pop_end(list_t *p_head,data_t *data);



//remove
status_t remove_beg(list_t * p_list);
status_t remove_end(list_t * p_list);
status_t remove_data(list_t *p_list, data_t e_data);

//lengtg

len_t list_length(list_t * p_list);
count_t count_all(list_t * p_list,data_t e_data);


//List Operation
list_t * array_to_list(int * in_array,size_t array_size);
status_t list_to_array(list_t * p_list,data_t ** pp_data,size_t * array_size);
list_t * get_concat_list(list_t * p_list1,list_t * p_list2);
list_t * get_reverse_list(list_t * p_list);
list_t * merge_list(list_t * p_list1,list_t * p_list2);
status_t reverse_list(list_t * p_list);
void append_list(list_t * p_list1,list_t * p_list2);

///Main

int main(int argc, char *argv[])
{
	data_t out_data;
	
	data_t * array=NULL;
	data_t  array1[6]={5,15,25,35,45,55};
	size_t array_size=0;
	list_t * p2=NULL;
	list_t * p3=NULL;
	list_t * p4=NULL;
	
	list_t *list1=create_list();
	list_t *list2=create_list();
	
	int i;
	
	
	status_t status;
	list_t * p= NULL;
	p=create_list();	
	insert_beg(p,10);
	insert_beg(p,20);
	insert_beg(p,30);
	insert_beg(p,40);
	insert_beg(p,50);
	
	display_list(p,"list after insert_beg()");

	insert_end(p,60);
	insert_end(p,70);
	insert_end(p,80);
	insert_end(p,80);
	insert_end(p,90);
	insert_end(p,100);
	
	display_list(p,"list after insert_end()");
	
	reverse_list(p);
	display_list(p,"list after reverse()");
	
	
	
	
	
	status=get_beg(p,&out_data);
	printf("GET BEG  STATUS : %d \n",status);
	printf("GET BEG  : %d \n",out_data);
	display_list(p,"list after get_beg()");
	
	
	status=get_end(p,&out_data);
	printf("GET END  STATUS : %d \n",status);
	printf("GET END  : %d \n",out_data);
	display_list(p,"list after get_end()");
	
	
	status=pop_beg(p,&out_data);
	printf("POP BEG  STATUS : %d \n",status);
	printf("POP BEG  : %d \n",out_data);
	display_list(p,"list after pop_beg()");
	
	
	status=pop_end(p,&out_data);
	printf("POP END  STATUS : %d \n",status);
	printf("POP END  : %d \n",out_data);
	display_list(p,"list after pop_end()");
	
	
    status=remove_beg(p);
    printf("REMOVE BEG  STATUS : %d \n",status);
	display_list(p,"list after remove_beg()");
	
	status=remove_end(p);
    printf("REMOVE END  STATUS : %d \n",status);
	display_list(p,"list after remove_end()");
	
	
	///list opeation
	
	status=list_to_array(p,&array,&array_size);
	printf("list_to_array() Status : %d\n",status);
	puts("List to Array() : ");
	
	for(i=0;i<array_size;i++)
	{
		printf("index %d : [%d]\n",i,array[i]);
	}
	
	
	p2=array_to_list(array1,6);
	puts("Array 2");
	for(i=0;i<6;i++)
	{
		printf("index %d : [%d]\n",i,array1[i]);
	}
	display_list(p2,"List 2");
	
	
	p3=get_concat_list(p,p2);
	display_list(p,"List 1");
	display_list(p2,"List 2");
	display_list(p3,"List 3 concat list");
	printf("List 3 length: %d\n",list_length(p3));

    
    for(i=1;i<=5;i++)
    {
    	insert_end(list1,i*10);
    	insert_end(list2,(i*10+5));
    }
    
    display_list(list1,"list 1 for merger sort");
    display_list(list2,"list 2 for merger sort");
    p4=merge_list(list1,list2);
    display_list(p4,"Merge list");

	
	puts("Mutable.Concat");
	append_list(p,p2);
	display_list(p,"List 1");
	
        
    status=remove_data(p,30);
    printf("REMOVE DATA(30) STATUS : %d \n",status);
    display_list(p,"list after remove_data(30)");
				
						
	printf("COUNT 80 : %d\n",count_all(p,80));
	
	printf("COUNT 20 : %d\n",count_all(p,20));
	
	printf("COUNT 120 : %d\n",count_all(p,120));
	
	printf("LIST LEN : %d\n",list_length(p));
	destroy_list(p);
	printf("LIST LEN : %d\n",list_length(p));
	
	
}


void * xmalloc(size_t size_in_bytes)
{
	node_t * p=NULL;
	p=malloc(size_in_bytes);
	assert(p!=NULL);
	return (p);
}

void generic_insert(node_t *p_beg,node_t * p_mid,node_t * p_end)
{
	p_mid->next = p_end;
	p_mid->prev = p_beg;
	p_beg->next = p_mid;
	p_end->prev = p_mid;	
}


void generic_delete(node_t * p_deleteNode)
{
	p_deleteNode->prev->next=p_deleteNode->next;
	p_deleteNode->next->prev=p_deleteNode->prev;	
	free(p_deleteNode);
}

node_t * get_list_node(data_t in_data)
{
	node_t * p=NULL;
	p=(node_t *)xmalloc(sizeof(node_t));
	p->next=NULL;
	p->prev=NULL;
	p->data=in_data;
	return (p);
}

node_t * locate_node(list_t *p_head,data_t search_data)
{
	node_t * p_run=p_head->next;
	
	while(p_run != p_head)
	{
		if(p_run->data==search_data)
			return(p_run);
			
		p_run=p_run->next;
	}
	
	return (NULL);
}

// Interface Routines

list_t * create_list()
{
	list_t * p=NULL;
	p=(list_t *)xmalloc(sizeof(list_t));
	p->data=0;
	p->next=p;
	p->prev=p;
	
	return (p);
}

status_t insert_beg(list_t *p_head, data_t in_data)
{
	generic_insert(p_head,get_list_node(in_data),p_head->next);
	return(SUCCESS);
}

status_t insert_end(list_t *p_head, data_t in_data)
{
	generic_insert(p_head->prev,get_list_node(in_data),p_head);
	return(SUCCESS);
}


status_t insert_after(list_t *p_head, data_t e_data,data_t in_data)
{
	node_t * p=NULL;
	p=locate_node(p_head,e_data);
	if(p == NULL)
	{
		return (DATA_NOT_FOUND);
	}
	generic_insert(p,get_list_node(in_data),p->next);
	return(SUCCESS);
}


status_t insert_before(list_t *p_head, data_t e_data,data_t in_data)
{
	node_t * p=NULL;
	p=locate_node(p_head,e_data);
	if(p == NULL)
	{
		return (DATA_NOT_FOUND);
	}
	generic_insert(p->prev,get_list_node(in_data),p);
	return(SUCCESS);
}


void display_list(list_t * p_head,char * msg)
{
	if(msg != NULL)
	printf("%s\n",msg);
	
	printf("[START]<->");
	
	node_t * p_run=p_head->next;
	
	while(p_run!= p_head)
	{
		printf("[%d]<->",p_run->data);
		p_run=p_run->next;
	}
	
	printf("[END]\n");
}

status_t reverse_list(list_t * p_list)
{
	node_t * p_run=p_list->next;
	node_t * p_end_original=p_list->prev;
	node_t * p_start_original=p_list->next;
	node_t * p_run_next=NULL;
	node_t * p_temp_link=NULL;
	
	while(p_run != p_list)
	{
		p_run_next=p_run->next;
		
		
		//puts("inside reverse while loop ");
		//printf("%d : ",p_run->data);
		//swap links 
		p_temp_link=p_run->prev;
		p_run->prev=p_run->next;
		p_run->next=p_temp_link;
		p_temp_link=NULL;
		p_run=p_run_next;
	}
	
	//swap links of head node
	p_list->next=p_end_original;
	p_list->prev=p_start_original;

	
	return (SUCCESS);
}

///GET & POP

status_t get_beg(list_t * p_list, data_t * o_data)
{
	if(p_list->next == p_list && p_list->prev == p_list)
		return(LIST_EMPTY);
	
	*o_data=p_list->next->data;

	return(SUCCESS);
}


status_t get_end(list_t * p_list, data_t * o_data)
{
	if(p_list->next == p_list && p_list->prev == p_list)
		return(LIST_EMPTY);
	
	*o_data=p_list->prev->data;

	return(SUCCESS);
}


status_t pop_beg(list_t * p_list, data_t * o_data)
{
	if(p_list->next == p_list && p_list->prev == p_list)
		return(LIST_EMPTY);
	
	*o_data=p_list->next->data;
	generic_delete(p_list->next);

	return(SUCCESS);
}


status_t pop_end(list_t * p_list, data_t * o_data)
{
	if(p_list->next == p_list && p_list->prev == p_list)
		return(LIST_EMPTY);
	
	*o_data=p_list->prev->data;
	generic_delete(p_list->prev);

	return(SUCCESS);
}





///

///remove 

status_t remove_data(list_t * p_list,data_t e_data)
{
	node_t *p = locate_node(p_list,e_data);
	if(p==NULL)
		return (DATA_NOT_FOUND);

	generic_delete(p);
	return(SUCCESS);
}


status_t remove_beg(list_t * p_list)
{
	if(p_list->next== p_list && p_list->prev==p_list)
		return(LIST_EMPTY);
		
	generic_delete(p_list->next);
	return(SUCCESS);
}


status_t remove_end(list_t * p_list)
{
	if(p_list->next== p_list && p_list->prev==p_list)
		return(LIST_EMPTY);
		
	generic_delete(p_list->prev);
	return(SUCCESS);
}


///


status_t destroy_list(list_t * p_list)
{
	node_t * p_run=p_list->next;
	node_t *p_run_next=NULL;
	while(p_run!=p_list)
	{
	p_run_next=p_run->next;
	generic_delete(p_run);
	//free(p_run);
    p_run=NULL;
	p_run=p_run_next;
	}	
	if(list_length(p_list)==0)
	     return(SUCCESS);
	else
	     return(FAILURE);
}

count_t count_all(list_t * p_list,data_t e_data)
{
	int elementcount=0;
	node_t *p_run=p_list->next;
	
	while(p_run != p_list)
	{
		if(p_run->data==e_data)
		{
			elementcount++;
		}
		p_run=p_run->next;
	}
	return(elementcount);
}


len_t list_length(list_t * p_list)
{
	int listcount=0;
	node_t * p_run=p_list->next;
	
	while(p_run != p_list)
	{
		listcount++;
		p_run=p_run->next;
	}
	return(listcount);
}


//list operation


list_t * array_to_list(int * in_array,size_t array_size)
{
	list_t * p_list=NULL;
	node_t * p_run=NULL;
	p_list=create_list();
	
	int i;
	for(i=0,p_run=p_list->next; i< array_size; i++, p_run=p_run->next)
	{
		insert_end(p_list,in_array[i]);
	}
	
	return (p_list);
}

status_t list_to_array(list_t * p_list,data_t ** pp_data,size_t * array_size)
{
	len_t listLength=list_length(p_list);
	data_t * array=(data_t *)xmalloc(listLength*sizeof(data_t));
	int i;
	node_t * p_run=NULL;
	
	
	for(i=0,p_run=p_list->next;i<listLength;i++,p_run=p_run->next)
	{
		array[i]=p_run->data;
	}
	
	*pp_data=array;
	*array_size=listLength;
	return (SUCCESS);
}

list_t * get_concat_list(list_t * p_list1,list_t * p_list2)
{
	list_t *p=create_list();
	node_t * p_run=NULL;
	
	//insert list 1 first
    for(p_run=p_list1->next;p_run != p_list1;p_run=p_run->next)
    	insert_end(p,p_run->data);
    
    		//insert list 2 
    	for(p_run=p_list2->next;p_run != p_list2;p_run=p_run->next)
    	insert_end(p,p_run->data);
    	
    	return(p) ;
}



list_t * merge_list(list_t * p_list1,list_t * p_list2)
{
	list_t *p=create_list();
	node_t * p_run1=p_list1->next;
	node_t * p_run2=p_list2->next;
	
	while(TRUE)
	{
	   if(p_run1 == p_list1)// check if first list is completed 
	   {
	   	while(p_run2!=p_list2)// if completed then dump list 2 as it is in resultant list
	   	{
	   		insert_end(p,p_run2->data);
	   		p_run2=p_run2->next;
	   	}
	   	break;
	   }
	   
	    if(p_run2 == p_list2)// check if second list is completed 
	   {
	   	while(p_run1!=p_list1)// if completed then dump list 1 as it is in resultant list
	   	{
	   		insert_end(p,p_run1->data);
	   		p_run1=p_run1->next;
	   	}
	   	break;
	   }
	   
	   if(p_run1->data<=p_run2->data)
	   {
	   	insert_end(p,p_run1->data);
	   	p_run1=p_run1->next;
	   }else
	   {
	   	insert_end(p,p_run2->data);
	   	p_run2=p_run2->next;
	   }
	   	
	}
    	
    	return(p) ;
}


void append_list(list_t *p_list1, list_t * p_list2)
{
	p_list1->prev->next=p_list2->next;
	p_list2->next->prev=p_list1->prev;
	p_list1->prev=p_list2->prev;
	p_list2->prev->next=p_list1;
	
	free(p_list2);
}
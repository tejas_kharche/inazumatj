//################################################
//      Name : Tejas										
//	  MSTC													 
//	  Assignment: Double Ended Queue USING DCLL	
//###############################################

#include <stdio.h>
#include <stdlib.h>

#define SUCCESS 									   1
#define FAILURE 										 0
#define DEQUEUE_EMPTY 							2
#define NULL_POINTER_EXCEPTION 	3
#define assert(CONDITION) do{\
													if(!(CONDITION))\
													{\
														puts("ASSERT MACRO:   CONDITION NOT SATISFIED");\
														exit(1);\
													}\
													}while(0)

typedef enum{FALSE=0,TRUE} bool;

typedef struct node{
	int data;
	struct node * next;
	struct node * prev;
}node_t,dequeue_t;

typedef int status_t;
typedef int data_t;
typedef int len_t;
 


//////// INTERNAL FUNCTIONS	//////////

status_t genericInsert(dequeue_t * p_beg, dequeue_t * p_mid, dequeue_t * p_end);
status_t genericDelete(dequeue_t * p_node);
void * xmalloc(size_t size_in_byte);

node_t * createNode(data_t in_data);


//////// INTERFACE FUNCTION	//////////
dequeue_t * createDEQueue();

bool isDEQueueEmpty(dequeue_t * p_dequeue);

status_t insertBeg(dequeue_t * p_dequeue,data_t in_data);
status_t insertEnd(dequeue_t * p_dequeue,data_t in_data);

status_t deleteBeg(dequeue_t * p_dequeue, data_t * p_out_data);
status_t deleteEnd(dequeue_t * p_dequeue, data_t * p_out_data);

status_t getBeg(dequeue_t * p_dequeue, data_t * p_out_data);
status_t getEnd(dequeue_t * p_dequeue, data_t * p_out_data);

status_t displayDEQueue(dequeue_t * p_dequeue, char * message);

status_t destroyDEQueue(dequeue_t **p_dequeue);

int main(int argc, char *argv[])
{
	status_t status;
	data_t data;
	dequeue_t * p_dequeue=createDEQueue();
	int i;
	
	for(i=1 ;i<=5;i++)
	{
		insertBeg(p_dequeue, i*10);
		displayDEQueue(p_dequeue,"DISPLAY DEQueue after insertBeg()\n");
		insertEnd(p_dequeue,i*10+5);
		displayDEQueue(p_dequeue,"DISPLAY DEQueue after insertEnd()\n");
	}
	
	
	displayDEQueue(p_dequeue,"\nDisplaying DEQUEUE \n");
	
	//while(!isDEQueueEmpty(p_dequeue))	
	for(i=0;i<5;i++)
	{	
		status=deleteBeg(p_dequeue,&data);
		if(status== SUCCESS)
		{
			printf("DELETE_BEG() ELEMENT : %d  \n",data);
			displayDEQueue(p_dequeue,"DEQueue After DELETE_BEG()\n");
		}
		puts("#############################################################");
		status=getBeg(p_dequeue,&data);
		if(status==SUCCESS)
		{
			printf(" GET_BEG() ELEMENT : %d \n ",data);
			displayDEQueue(p_dequeue,"DEQueue After GET_BEG()\n");
		}
	}
	
	
	for(i=0;i<5;i++)
	{	
		status=deleteEnd(p_dequeue,&data);
		if(status== SUCCESS)
		{
			printf("DELETE_END() ELEMENT : %d  \n",data);
			displayDEQueue(p_dequeue,"DEQueue After DELETE_END()\n");
		}
		puts("#############################################################");
		status=getEnd(p_dequeue,&data);
		if(status==SUCCESS)
		{
			printf(" GET_END() ELEMENT : %d \n ",data);
			displayDEQueue(p_dequeue,"DEQueue After GETE_END()\n");
		}
	}
	
	
	destroyDEQueue(&p_dequeue);
	
	//jsut checkigng if the queue is destroyed or  not
	if(p_dequeue == NULL)
		printf("DEQUEUE DESTROYED");
	else
		printf("DEQUEUE NOT DESTROYED");
		
}

status_t genericInsert(dequeue_t * p_beg, dequeue_t * p_mid, dequeue_t * p_end)
{
	p_mid->next=p_end;
	p_mid->prev=p_beg;	
	p_beg->next=p_mid;
	p_end->prev=p_mid;
			
	return (SUCCESS);	
}

status_t genericDelete(dequeue_t * p_node)
{
	p_node->prev->next=p_node->next;
	p_node->next->prev=p_node->prev;
	p_node->next=NULL;
	p_node->prev=NULL;
	free(p_node);
	p_node=NULL;		
	return (SUCCESS);
}


bool isDEQueueEmpty(dequeue_t * p_dequeue)
{
 	if (p_dequeue->next == p_dequeue && p_dequeue->prev == p_dequeue)
 		return (TRUE);
 	else
 		return (FALSE); 
}

void * xmalloc(size_t size_in_byte)
{
	void * p=NULL;
	p=malloc(size_in_byte);
	assert(p != NULL);
	return (p);	
}

dequeue_t * createDEQueue()
{
	node_t * p_dequeue=NULL;
	p_dequeue=(dequeue_t *)xmalloc(sizeof(dequeue_t));
	p_dequeue->data=0;
	p_dequeue->next=p_dequeue;
	p_dequeue->prev=p_dequeue;
	
	return(p_dequeue);
}

node_t * createNode(data_t in_data)
{
	node_t * p=NULL;
	p=(node_t *)xmalloc(sizeof(node_t));
	p->data=in_data;
	p->prev=NULL;
	p->next=NULL;
	
	return(p);
}

status_t insertBeg(dequeue_t * p_dequeue,data_t in_data)
{
	if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	genericInsert(p_dequeue,createNode(in_data),p_dequeue->next);	
	return(SUCCESS);
}


status_t insertEnd(dequeue_t * p_dequeue,data_t in_data)
{
		if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
		}
	genericInsert(p_dequeue->prev,createNode(in_data),p_dequeue);	
	return(SUCCESS);
}

status_t deleteBeg(dequeue_t * p_dequeue, data_t * p_out_data)
{
	if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isDEQueueEmpty(p_dequeue)){
		return(DEQUEUE_EMPTY);
	}
	
	*p_out_data=p_dequeue->next->data;
	genericDelete(p_dequeue->next);
	return(SUCCESS);
}
status_t deleteEnd(dequeue_t * p_dequeue, data_t * p_out_data)
{
	if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isDEQueueEmpty(p_dequeue)){
		return(DEQUEUE_EMPTY);
	}
		
	*p_out_data=p_dequeue->prev->data;
	genericDelete(p_dequeue->prev);
	return(SUCCESS);
}

status_t getBeg(dequeue_t * p_dequeue, data_t * p_out_data)
{
	if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isDEQueueEmpty(p_dequeue)){
			return(DEQUEUE_EMPTY);
	}
	
	*p_out_data=p_dequeue->next->data;
	return(SUCCESS);
}

status_t getEnd(dequeue_t * p_dequeue, data_t * p_out_data)
{
	if(p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	if(isDEQueueEmpty(p_dequeue)){
		return(DEQUEUE_EMPTY);
	}
		
	*p_out_data=p_dequeue->prev->data;
	return(SUCCESS);
}

status_t destroyDEQueue(dequeue_t **p_dequeue)
{
	if(*p_dequeue==NULL){
		//printf("NULL POINTER EXCEPTION");
		return(NULL_POINTER_EXCEPTION);
	}
	
	node_t * p_run=(*p_dequeue)->next;
	node_t * p_run_next=NULL;
	while(p_run != *p_dequeue)
	{
		p_run_next=p_run->next;
		genericDelete(p_run);
		p_run=p_run_next;
	}
	
	free(*p_dequeue);
	*p_dequeue=NULL;
	
	return(SUCCESS);
	
}

status_t displayDEQueue(dequeue_t * p_dequeue, char * message)
{
	if(p_dequeue == NULL){
		return(NULL_POINTER_EXCEPTION);	
	}
	
	if(message != NULL){
		printf("%s",message);
	}
	
	node_t * p_run=p_dequeue->next;
	printf("[START]<->");
	while(p_run != p_dequeue)
	{
		printf("[%d]<->",p_run->data);
		p_run=p_run->next;
	}
	puts("[END]");
	
	return(SUCCESS);
}
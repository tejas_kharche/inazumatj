// 1.
#include <windows.h>
#include <stdio.h>
#include "credits.h"

// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "Winmm.lib")

#define TJ_POINT_RADIUS 3

#define ID_TIMER_SNOW 909
#define ID_TIMER_CREDITS 910

COLORREF red=RGB(255,0,0);
COLORREF green=RGB(0,255,0);
COLORREF blue=RGB(0,0,255);
COLORREF white=RGB(255,255,255);


int CLIENT_XArea,CLIENT_YArea;
int CLIENT_XMouse,CLIENT_YMouse;
int SPAWN_RATE=3;
int particle_count=0;
const int POINT_SPAWN_X=100;
const int POINT_SPAWN_Y=0;

int IS_GROUP_NAME_DISPLAYED=FALSE;
int IS_THANKS_MSG_DISPLAYED=FALSE;

const int V_SCROLL_MAX_Y=200;
const int V_SCROLL_MIN_Y=600;

const int V_SCROLL_FACTOR=50;
const int IN_LINE_SPACE=50;
//int CREDIT_SPAWN_POINT_X=550;

int CREDIT_SPAWN_POINT_X=700;
int CREDIT_SPAWN_POINT_Y=600;

const int no_of_participants=14;

typedef struct tagParticipants{
	char * name;
	int x;
	int y;
} groupMembers;

//groupMembers * memberArr[3];

//
/*
memberArr[0].name="Tejas Kharche";
memberArr[0].x=0;
memberArr[0].y=0;


memberArr[1].name="Yogeshwar Rasal";
memberArr[1].x=0;
memberArr[1].y=0;

memberArr[1].name="Prathamesh Bhandare";
memberArr[1].x=0;
memberArr[1].y=0;


*/

groupMembers m1={"Kamlesh Patil",0,0};
groupMembers m2={"Atharva Bhatkhalkar",0,0};
groupMembers m3={"Swapnil Bobde",0,0};
groupMembers m4={"Onkar Girkar",0,0};
groupMembers m5={"Yashodeep Khadse",0,0};
groupMembers m6={"Ganesh Kadam",0,0};
groupMembers m7={"Hrushikesh Adya",0,0};
groupMembers m8={"Rushabh Patil",0,0};
groupMembers m9={"Prathamesh Bhandare",0,0};
groupMembers m10={"Shrinivas Dongaonkar",0,0};
groupMembers m11={"Datta Jadhav",0,0};
groupMembers m12={"Chaitanya Kulkarni",0,0};
groupMembers m13={"Yogeshwar Rasal",0,0};
groupMembers m14={"Tejas Kharche",0,0};


//groupMembers * memberArr[]={&m1,&m2,&m3};
groupMembers memberArr[]={m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14};


/*
memberArr[0]=&m1;
memberArr[1]=&m2;
memberArr[2]=&m3;
*/




typedef struct tagmyPoint
{
    int xPosition;
    int yPosition;
    int xVelocity;
    int yVelocity;
    //int xAcceleration;
    //int yAcceleration;
    int dead;  //by default when myPoint spawns it is not dead  
    COLORREF * color; 
}TJ_POINT,*PTJ_POINT,**PPTJ_POINT;

typedef struct tagVector
{
    PPTJ_POINT list;
    //tagmyPoint ** list;
	int count;
}VECTOR,*PVECTOR;

PVECTOR CreateVector(void);
int PushElement(PVECTOR pVector,PTJ_POINT pPoint);
//void DestroyVector(PVECTOR pVector);
void DestroyVector(PVECTOR * pVector);
void Advance(PTJ_POINT pPoint);
void Accelerate(PTJ_POINT pPoint,int xAccel,int yAccel);
void SpawnLoop(PVECTOR pVector);
void PopElement(PVECTOR pVector,int index);

//int IsDead(PTJ_POINT pPoint);


//Scroll logic
void credit_offset_calculation(groupMembers * array);


PVECTOR PointVector;


PVECTOR CreateVector(void)
{
    PVECTOR pVector=NULL;
    pVector=(PVECTOR)malloc(sizeof(VECTOR));
    pVector->list=NULL;
    pVector->count=0;
    return pVector;
}

/*
void DestroyVector(PVECTOR pVector)
{
    int i=0;
    for (i=0;i<pVector->count;i++)
    {
        free(pVector->list[i]);
		pVector->list[i]=NULL;
    }
    free(pVector->list);
    pVector->list=NULL;
    pVector->count=0;
    free(pVector);
    pVector=NULL;
}
*/


int PushElement(PVECTOR pVector,PTJ_POINT pPoint)
{
    pVector->count+=1;
    pVector->list=(PPTJ_POINT)realloc(pVector->list,pVector->count*sizeof(PTJ_POINT));
	
    pVector->list[pVector->count-1]=pPoint;
	
    return (EXIT_SUCCESS);
}


void PopElement(PVECTOR pVector,int index)
{
    if(index <0 || index >= pVector->count)
		return;
	
	free(pVector->list[index]);
	pVector->list[index] = NULL;
	
	int i;
	for(i=index;i< pVector->count-1 ;i++)
	{
		pVector->list[i]=pVector->list[i+1];
		pVector->list[i+1] = NULL;
	}
	
	pVector->count-=1;
}


PTJ_POINT CreatePoint(int xPos,int yPos, int xVel, int yVel,COLORREF * in_color)
{
    PTJ_POINT pPoint=NULL;
	pPoint=(PTJ_POINT)malloc(sizeof(TJ_POINT));
    pPoint->xPosition=xPos;
    pPoint->yPosition=yPos;
    pPoint->xVelocity=xVel;
    pPoint->yVelocity=yVel;
	pPoint->dead=0;
	pPoint->color=in_color;
	
    return pPoint;
}


void Accelerate(PTJ_POINT pPoint,int xAcc,int yAcc)
{
    pPoint->xVelocity+=xAcc;
    pPoint->yVelocity+=yAcc;
}


void Advance(PTJ_POINT pPoint){
    pPoint->xPosition+=pPoint->xVelocity;
    pPoint->yPosition+=pPoint->yVelocity;

	if(pPoint->xPosition<=0 || pPoint->xPosition>=CLIENT_XArea || pPoint->yPosition<=0 || pPoint->yPosition>=CLIENT_YArea)
	{
		pPoint->dead=1;	
	}
}	


void SpawnLoop(PVECTOR pVector)
{
    int i=0;
    int xVel;
    int yVel;
	int xAccel;
    int yAccel;
	int RandomNo;
	
	
	
	
    PTJ_POINT pPoint;
    for(i=0;i<SPAWN_RATE;i++)
    {
        
		xVel=rand()%2;
        yVel=rand()%2;  
		
		xAccel=rand()%2;
        yAccel=rand()%2;
		
		xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
		
	   
	   
		RandomNo=rand();
		
		/*
		xVel=RandomNo%5;
        yVel=RandomNo%5;  
		
		xAccel=RandomNo%3;
        yAccel=RandomNo%3;
		
		xAccel=((RandomNo%2==0)?xAccel:-1*xAccel);
		yAccel=((RandomNo%2!=0)?yAccel:-1*yAccel);
		*/
	
		/*
			if(RandomNo%3 == 0)
			{
				pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel,&red);   //point will be created at current mouse position
			}else if(RandomNo%3 == 1)
			{
				pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel,&green);   //point will be created at current mouse position
			}else if(RandomNo%3 == 2)
			{
				pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel,&blue);   //point will be created at current mouse position
			}
			*/
			
		//pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel,&white); 
		pPoint=CreatePoint(POINT_SPAWN_X,POINT_SPAWN_Y,xVel,yVel,&white); 
	
	
       // pPoint=CreatePoint(CLIENT_XMouse,CLIENT_YMouse,xVel,yVel);   //point will be created at current mouse position
        //pPoint=CreatePoint(CLIENT_XArea,CLIENT_YArea,xVel,yVel);  //All points will start from screen's center 
        PushElement(pVector,pPoint);
        particle_count+=1;  //This particle_count  is a global var which act as a Counter for total active particle which were spawned
    }
	//pVector=NULL;
}


void AdvanceLoop(PVECTOR pVector)
{
    int i=0;
    int xAccel;
    int yAccel;
	int RandomNumber;
    //PTJ_POINT pPoint;
	
    for (i=0;i<pVector->count;i++)
    {
        //if(PointVector->list[i]==NULL)
		//continue;
		
		
        xAccel=rand()%2;
        yAccel=rand()%2;
		
		xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
    
	
		
		
		
		RandomNumber=rand();
		
		/*
		xAccel=RandomNumber%3;
        yAccel=RandomNumber%3;
		
		xAccel=((RandomNumber%2==0)?xAccel:-1*xAccel);
		yAccel=((RandomNumber%2!=0)?yAccel:-1*yAccel);
		*/
	
		Advance(pVector->list[i]);
		
		Accelerate(pVector->list[i],xAccel,yAccel);
		
        if(pVector->list[i]->dead==1)
        {
            PopElement(pVector,i);
            particle_count-=1;
        }

    } 
}

//2. Hollywood Principal
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

///MY Circle

bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc);



//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{

    //My Addon

     PointVector=CreateVector();  //Create Point Vector

	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC"); // name of class 
    TCHAR szWindowCaption[] = TEXT("104_Tejas : BASIC PARTICLE SYSTEM");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_QUESTION); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	wndclass.lpszClassName = szAppName; // Group Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// 1st 
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	
	//Title less window
	SetWindowLong(hwnd,GWL_STYLE,0);
	SetWindowPos(hwnd, HWND_TOP , 0, 0, 1920, 1080, SWP_FRAMECHANGED);
	
	
	//ShowWindow(hwnd, nCmdShow);
	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);


		//TEsting
		//SpawnLoop(PointVector);	
		//AdvanceLoop(PointVector);
        //InvalidateRect(hwnd, NULL, TRUE);


//testin end
	}

	// 10 Safe Release
    //DestroyVector(PointVector); //Destroy Point Vector
	

	PointVector=NULL;
	return((int)msg.wParam);
}


// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b

    
    static int cxClient, cyClient, cxChar, cyChar, cxMouse,cyMouse;
    HDC hdc = NULL; 

	//Splash screen//

	HDC hdcComp;
	HINSTANCE hInst;
	static HBITMAP hbmap;
	HGDIOBJ prevHGDIObj = NULL;
	BITMAP bmBuffer;
	RECT rc;

	////////

    TEXTMETRIC tm; 
    PAINTSTRUCT ps; 
	int xAccel;
    int yAccel;
	int pointIndex;
	int RandomNo;
	static int IS_SPLASH_SCREEN_DRAWN=FALSE;
	int i;
    switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
			//MessageBox(NULL, TEXT("Window Created!"), TEXT("1st WND"), MB_OK);
	       	SetTimer(hwnd, ID_TIMER_SNOW,67, NULL);
			SetTimer(hwnd, ID_TIMER_CREDITS,4000,NULL);
            //While testing
			//SetTimer(hwnd, ID_TIMER_CREDITS,500,NULL);
			hdc = GetDC(hwnd); 
            //Splash screen specific
			PlaySound(TEXT("credits.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP );
			hInst = (HINSTANCE)((LPCREATESTRUCT)lParam)->hInstance;
			hbmap = LoadBitmap(hInst, MAKEINTRESOURCE(MY_BITMAP));
			///////Splash screen specific ////////
			
			GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; 
            cyChar = tm.tmHeight + tm.tmExternalLeading; 





            ReleaseDC(hwnd, hdc); 
            hdc = NULL; 
    	break;

        case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
            CLIENT_XArea = cxClient;
            CLIENT_YArea = cyClient;
			
			//CREDIT_SPAWN_POINT_X=cxClient;
			//CREDIT_SPAWN_POINT_Y=cyClient;
            break; 


        case WM_MOUSEMOVE:
            cxMouse = LOWORD(lParam);
            cyMouse = HIWORD(lParam);
			CLIENT_XMouse=cxMouse;
			CLIENT_YMouse=cyMouse;
            
            break;

        case WM_PAINT: 
            hdc = BeginPaint(hwnd, &ps); 
            
			
			HPEN penOld;
			HPEN penParticle;
			HFONT HOldFont,HNewFont,HNewFont_BIG;

		//Create new FONT
			HNewFont=CreateFont(50, 0,0,0, FW_ULTRABOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Cambria"));
			
			HNewFont_BIG=CreateFont(70, 0,0,0, FW_ULTRABOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Cambria"));
			
			HOldFont=(HFONT)SelectObject(hdc, HNewFont);
			
			
	
			//Splash Screen
				hdcComp = CreateCompatibleDC(hdc);
				prevHGDIObj = SelectObject(hdcComp, hbmap);
				GetObject(hbmap, sizeof(BITMAP), &bmBuffer); // retrieves dimension of a bitmap
				GetClientRect(hwnd, &rc);  // fill the x,y, width, height into rectangle
				BitBlt(hdc, (rc.right - bmBuffer.bmWidth)/2, (rc.bottom - bmBuffer.bmHeight)/2, (int)bmBuffer.bmWidth , (int)bmBuffer.bmHeight, hdcComp, 0, 0, SRCCOPY); // performs bit block transfer from memory DC to hdc returned from BeginPaint
				SelectObject(hdcComp, prevHGDIObj);
				IS_SPLASH_SCREEN_DRAWN=TRUE;
			//Splash screen///
			
			
			
			//penOld=(HPEN) SelectObject(hdc,penParticle);
			
			//Render loop
			for(pointIndex=0; pointIndex < PointVector->count; pointIndex++)
            {
				HPEN penParticle;
				penParticle=CreatePen(PS_SOLID, 3,*(PointVector->list[pointIndex]->color) );
				penOld=(HPEN) SelectObject(hdc,penParticle);
				MyCircle( PointVector->list[pointIndex]->xPosition, PointVector->list[pointIndex]->yPosition, TJ_POINT_RADIUS , hdc);	
				SelectObject(hdc,penOld);
				DeleteObject(penParticle);
			}
            
			
			//////Credits display
			
			for(i=0;i<no_of_participants;i++)
			{
				//SetTextAlign(hdc,TA_LEFT | TA_TOP);
				//TextOut(hdc,0,0,(memberArr[i]).name,lstrlen((memberArr[i]).name));
				
				//TextOut(hdc,0,0,m1.name,lstrlen(m1.name));
				//TextOut(hdc,0,0,"YO",lstrlen("YO"));
				if((memberArr[i].y+CREDIT_SPAWN_POINT_Y) > V_SCROLL_MAX_Y && (memberArr[i].y+CREDIT_SPAWN_POINT_Y) < (V_SCROLL_MIN_Y ))
				{	
					SetBkMode(hdc,TRANSPARENT);
					//SetTextColor(hdc,RGB(220,20,60));
					SetTextColor(hdc,RGB(255,0,0));
					SetTextAlign(hdc,TA_LEFT|TA_TOP);
					TextOut(hdc,CREDIT_SPAWN_POINT_X+ memberArr[i].x,CREDIT_SPAWN_POINT_Y+ memberArr[i].y+ IN_LINE_SPACE,memberArr[i].name,lstrlen(memberArr[i].name));
					
				}
				
			}
			
			/*
			if((memberArr[13].y+CREDIT_SPAWN_POINT_Y) < (V_SCROLL_MAX_Y ))
			{
				if(IS_THANKS_MSG_DISPLAYED==FALSE)
				{
					SetBkMode(hdc,TRANSPARENT);
					SetTextColor(hdc,RGB(255,0,0));
					SetTextAlign(hdc, TA_CENTER);
					TextOut(hdc,CLIENT_XArea/2 ,CLIENT_YArea/2-(cyChar/2)+ memberArr[i].y+ IN_LINE_SPACE,"Thank You!",lstrlen("Thank You!"));
					IS_THANKS_MSG_DISPLAYED=TRUE;
				}else{
					SetBkMode(hdc,TRANSPARENT);
					SetTextColor(hdc,RGB(255,0,0));
					SetTextAlign(hdc, TA_CENTER);
					TextOut(hdc,CLIENT_XArea/2 ,CLIENT_YArea/2-(cyChar/2)+ memberArr[i].y+ IN_LINE_SPACE,"Hon. Linus Torvalds Group",lstrlen("Hon. Linus Torvalds Group"));
				}
			}
				*/
			if((memberArr[13].y+CREDIT_SPAWN_POINT_Y) < (V_SCROLL_MAX_Y ))
			{
				(HFONT)SelectObject(hdc, HNewFont_BIG);
				if((memberArr[13].y+CREDIT_SPAWN_POINT_Y+V_SCROLL_FACTOR) < (V_SCROLL_MAX_Y ))
				{
					SetBkMode(hdc,TRANSPARENT);
					SetTextColor(hdc,RGB(255,0,0));
					SetTextAlign(hdc, TA_CENTER);
					TextOut(hdc,CLIENT_XArea/2 ,600+ memberArr[i].y+ IN_LINE_SPACE,"Hon. Linus Torvalds Group",lstrlen("Hon. Linus Torvalds Group"));
					IS_THANKS_MSG_DISPLAYED=TRUE;
				}else{
					SetBkMode(hdc,TRANSPARENT);
					SetTextColor(hdc,RGB(255,0,0));
					SetTextAlign(hdc, TA_CENTER);
					TextOut(hdc,CLIENT_XArea/2 ,600+ memberArr[i].y+ IN_LINE_SPACE,"Thank You!",lstrlen("Thank You!"));
				}
			
			}
			
			SelectObject(hdc, HOldFont);  //Restore old font
			EndPaint(hwnd, &ps); 
            hdc = NULL; 
            break;


     case WM_TIMER:
		switch(wParam)
        {
			case ID_TIMER_SNOW:
				SpawnLoop(PointVector);	
				AdvanceLoop(PointVector);
				//InvalidateRect(hwnd, NULL, TRUE);
				//RedrawWindow(hwnd,NULL,NULL,RDW_ERASE | RDW_INVALIDATE);

			RedrawWindow(hwnd,NULL,NULL, RDW_INTERNALPAINT | RDW_NOERASE | RDW_INVALIDATE);
			break;
			case ID_TIMER_CREDITS:
				//memberArr[0].y-=V_SCROLL_FACTOR;
				if( IS_GROUP_NAME_DISPLAYED == FALSE && IS_THANKS_MSG_DISPLAYED == TRUE);
					IS_GROUP_NAME_DISPLAYED=TRUE;
				memberArr[0].y-=V_SCROLL_FACTOR;
				credit_offset_calculation(memberArr);
				break;
		}
		break;

	case WM_KEYDOWN:
		if(wParam == VK_ESCAPE )
			{
				KillTimer(hwnd,ID_TIMER_SNOW);
				KillTimer(hwnd,ID_TIMER_CREDITS);
				PostQuitMessage(0);
			}
		break;
	case WM_DESTROY:
		//MessageBox(NULL, TEXT("Window Destroyed"), TEXT("1st WND Going"), MB_ICONERROR);
		 KillTimer(hwnd,ID_TIMER_SNOW);
		 KillTimer(hwnd,ID_TIMER_CREDITS);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void DestroyVector(PVECTOR * pVector)
{
    int i=0;
    for (i=0;i<(*pVector)->count;i++)
    {
        free((*pVector)->list[i]);
		(*pVector)->list[i]=NULL;
    }
    free((*pVector)->list);
    (*pVector)->list=NULL;
    (*pVector)->count=0;
    free((*pVector));
    (*pVector)=NULL;
}


bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc)
{
    return(Ellipse(hdc,(iXCenter-radius),(iYCenter-radius),(iXCenter+radius),(iYCenter+radius)));
}


void credit_offset_calculation(groupMembers * array)
{
	int size=no_of_participants;
	//int size=sizeof(array)/sizeof(array[0]);
	int i=0;
	for(i=0;i<size;i++)
	{
		if(i==0)
			continue;
		else
			array[i].y=array[i-1].y+V_SCROLL_FACTOR;	
	}
}
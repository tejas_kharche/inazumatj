//@Author : Tejas Kharche //
//  Capture Screenshot //

#include <windows.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")

#define SNAPSHOT_BUTTON 300

char * ImagePath="CapturedImage/snapshot.bmp";

//2. Hollywood Principal

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL SaveBitmap(HDC hDC,HBITMAP hBitmap,char* szPath);
void TakeSnap(HWND hWnd);


int CLIENT_AREA_X;
int CLIENT_AREA_Y;

//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{
	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC"); // name of class 
    TCHAR szWindowCaption[] = TEXT("104_Tejas : SNAPSHOT BUTTON");

	
	
	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_QUESTION); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(DKGRAY_BRUSH);

	wndclass.lpszClassName = szAppName; // Group Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// 1st 
		NULL);						// No lpParam (used for additional data)


	
	HWND hScreenShotButton =NULL; //handle to button
	
	hScreenShotButton = CreateWindow("BUTTON", "SNAPSHOT", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 
                            0, 0, 100, 50, hwnd,(HMENU)SNAPSHOT_BUTTON, hInstance, NULL);


	// 8  memset
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// 10
	return((int)msg.wParam);
}

// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b
    static int cxClient, cyClient, cxChar, cyChar; 
                          
    HDC hdc = NULL; 
    TEXTMETRIC tm; 
    PAINTSTRUCT ps; 
	
	switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		//MessageBox(NULL, TEXT("Window Created!"), TEXT("1st WND"), MB_OK);
	       
            hdc = GetDC(hwnd); 
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; 
            cyChar = tm.tmHeight + tm.tmExternalLeading; 
            ReleaseDC(hwnd, hdc); 
            hdc = NULL; 
    	break;

        case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
			
			CLIENT_AREA_X=cxClient;
			CLIENT_AREA_Y=cyClient;
			
            break; 
			
		 case WM_COMMAND:
            if (LOWORD(wParam) == SNAPSHOT_BUTTON)
            {
                //displayDialog_1(hWnd);
				TakeSnap(hwnd);
            }
			break;
			
		case WM_KEYDOWN:
			if(wParam == 'S' || wParam == 's')
				TakeSnap(hwnd);
			break;

		// Last msg
	case WM_DESTROY:
		//MessageBox(NULL, TEXT("Window Destroyed"), TEXT("1st WND Going"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void TakeSnap(HWND hWnd)
{	
	//int width = GetSystemMetrics(SM_CXSCREEN);//1920;//GetDeviceCaps(hScreenDC,HORZRES);    //this works i think it misbehave due to scaling in windows 
	//int height = GetSystemMetrics(SM_CYSCREEN);//1080;//GetDeviceCaps(hScreenDC,VERTRES);   //this works i think it misbehave due to scaling in windows 
	
	//int width = CLIENT_AREA_X;
	//int height = CLIENT_AREA_Y;
	
	int width = 1920;  
	int height = 1080;
	
	/*
	RECT rect;
	GetWindowRect(hWnd, &rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;
	*/
	
	HDC hScreenDC = GetDC(nullptr); // CreateDC("DISPLAY",nullptr,nullptr,nullptr);
	HDC hMemoryDC = CreateCompatibleDC(hScreenDC);
	
	HBITMAP hBitmap = CreateCompatibleBitmap(hScreenDC,width,height);
	HBITMAP hOldBitmap = (HBITMAP)SelectObject(hMemoryDC,hBitmap);
	
	BitBlt(hMemoryDC,0,0,width,height,hScreenDC,0,0,SRCCOPY);  //if I want to capture the 1920x1080 image startingfrom (0,0)
	//BitBlt(hMemoryDC,0,0,rect.right,rect.bottom,hScreenDC,rect.left,rect.top,SRCCOPY); 
	
	
	
	//StretchBlt(hMemoryDC,0,0,GetSystemMetrics(SM_CXSCREEN),GetSystemMetrics(SM_CYSCREEN),hScreenDC,0,0,width,height,SRCCOPY);
	
	hBitmap = (HBITMAP)SelectObject(hMemoryDC,hOldBitmap);
	SaveBitmap(hMemoryDC, hBitmap,ImagePath);
	DeleteDC(hMemoryDC);
	DeleteDC(hScreenDC);
}

BOOL SaveBitmap(HDC hDC,HBITMAP hBitmap,char* szPath)
{
	DWORD error;
	FILE * fp= NULL;
	fp = fopen(szPath,"wb");
	if(fp == NULL)
	{
		return false;
	}
	BITMAP Bm;
	BITMAPINFO BitInfo;
	ZeroMemory(&BitInfo, sizeof(BITMAPINFO));
	BitInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	BitInfo.bmiHeader.biBitCount = 0;

	if(!::GetDIBits(hDC, hBitmap, 0, 0, NULL, &BitInfo, DIB_RGB_COLORS))
	{
		return (false);
	}
	Bm.bmHeight = BitInfo.bmiHeader.biHeight;
	Bm.bmWidth  = BitInfo.bmiHeader.biWidth;

	BITMAPFILEHEADER    BmHdr;

	BmHdr.bfType        = 0x4d42;   // 'BM' WINDOWS_BITMAP_SIGNATURE
	BmHdr.bfSize        = (((3 * Bm.bmWidth + 3) & ~3) * Bm.bmHeight) 
		+ sizeof(BITMAPFILEHEADER) 
		+ sizeof(BITMAPINFOHEADER);
	BmHdr.bfReserved1    = BmHdr.bfReserved2 = 0;
	BmHdr.bfOffBits      = (DWORD) sizeof(BITMAPFILEHEADER) 
		+ sizeof(BITMAPINFOHEADER);

	BitInfo.bmiHeader.biCompression = 0;
	// Writing Bitmap File Header ////
	size_t size = fwrite(&BmHdr,sizeof(BITMAPFILEHEADER),1,fp);
	if(size < 1)
	{
		error = GetLastError();
	}
	size = fwrite(&BitInfo.bmiHeader,sizeof(BITMAPINFOHEADER),1,fp);
	if(size < 1)
	{		
		error = GetLastError();
	}
	BYTE *pData = new BYTE[BitInfo.bmiHeader.biSizeImage + 5];
	if(!::GetDIBits(hDC, hBitmap, 0, Bm.bmHeight, 
		pData, &BitInfo, DIB_RGB_COLORS))
		return (false);
	if(pData != NULL)
		fwrite(pData,1,BitInfo.bmiHeader.biSizeImage,fp);

	fclose(fp);
	delete (pData);
	return (true);
}

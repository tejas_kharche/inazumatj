#define ID_TIMER_BST_DISPLAY   901

#define FAILURE  		 0
#define SUCCESS 		 1
#define BST_DATA_NOT_FOUND 	 2 
#define BST_NO_SUCCESSOR 	 3 
#define BST_NO_PREDECESSOR 	 4 
#define BST_EMPTY 		 5
#define MEMORY_ALOCATION_FAILED  6


typedef int data_t;
typedef int status_t;
typedef int ret_t;



typedef struct tagBstNode
{
	data_t data;
	struct tagBstNode * parent;
	struct tagBstNode * left;
	struct tagBstNode * right;
	HBITMAP hbmap;
	int x;
	int y;
	int height;
}bst_node_t;

typedef struct tagBst
{
	bst_node_t * p_root;
	int nr_element;
}bst_t;



int CLIENT_CHAR_X,CLIENT_CHAR_Y;
int TREE_X_OFFSET=90;
int TREE_Y_OFFSET=90;

int TREE_SHIFT_X_OFFSET=50;
int TREE_SHIFT_Y_OFFSET=50;
int TREE_SPAWN_X=800;
int TREE_SPAWN_Y=100;


int ARTIST_PHOTO_RADIUS=35;

int POST_ORDER_DISPLAYED=FALSE;
int IN_ORDER_DISPLAYED=FALSE;
int PRE_ORDER_DISPLAYED=FALSE;


int IS_TREE_DIPLAYED=0;
int TREE_TRAVERSAL_MODE=1; //by default it is pre_order
int NUM_BST_NODE_DISPLAYED=0;

//constants

int ORG_PRE_ORDER_START_X_POSITION=150;
int ORG_PRE_ORDER_START_Y_POSITION=500;

int ORG_IN_ORDER_START_X_POSITION=150;
int ORG_IN_ORDER_START_Y_POSITION=530;

int ORG_POST_ORDER_START_X_POSITION=150;
int ORG_POST_ORDER_START_Y_POSITION=560;


//Modifying global vaiable

int PRE_ORDER_START_X_POSITION=ORG_PRE_ORDER_START_X_POSITION;
int PRE_ORDER_START_Y_POSITION=ORG_PRE_ORDER_START_Y_POSITION;

int IN_ORDER_START_X_POSITION=ORG_IN_ORDER_START_X_POSITION;
int IN_ORDER_START_Y_POSITION=ORG_IN_ORDER_START_Y_POSITION;

int POST_ORDER_START_X_POSITION=ORG_POST_ORDER_START_X_POSITION;
int POST_ORDER_START_Y_POSITION=ORG_POST_ORDER_START_Y_POSITION;


bst_t * create_bst();
//status_t tree_insert(bst_t * p_bst,data_t in_data);
//status_t tree_insert(HDC hdc,bst_t* p_bst, data_t in_data);
status_t tree_insert(HDC hdc,bst_t* p_bst, data_t in_data,int image_id);

void w_inorder_r(HDC hdc,bst_t * p_bst);
void w_preorder_r(HDC hdc,bst_t * p_bst);
void w_postorder_r(HDC hdc,bst_t * p_bst);
status_t w_destroy_bst(bst_t **pp_bst);
//void w_bst_display_r(HDC hdc,bst_t * p_bst);
void w_bst_display_r(HINSTANCE hInst,HDC hdc,bst_t * p_bst);
//Auxilary FUNCTIONS

bst_node_t * getBstNode(int in_data);
void * xmalloc(size_t size_in_bytes);
int w_find_max_height(bst_t *p_bst);
//bool search_bst(bst_t *p_bst, data_t search_data); 

/*
ret_t maximum_bst(bst_t *p_bst, data_t *p_max_data); 
ret_t minimum_bst(bst_t *p_bst, data_t *p_min_data); 
ret_t inorder_successor(bst_t *p_bst, data_t e_data, data_t *p_succ_data); 
ret_t inorder_predecessor(bst_t *p_bst, data_t e_data, data_t *p_pred_data); 
*/

/* BST Auxillary */ 
//bst_node_t *search_bst_node(bst_node_t *p_root, data_t search_data); 
//bst_node_t *inorder_successor_node(bst_node_t *p_bst_node); 
//bst_node_t *inorder_predecessor_node(bst_node_t *p_bst_node); 
//void _w_bst_display_r(HDC hdc,bst_node_t * p_node);
void _w_bst_display_r(HINSTANCE hInst,HDC hdc,bst_node_t * p_node);
void _w_find_max_height_r(bst_node_t * p_node,int * o_max);
void _w_inorder_r(HDC hdc,bst_node_t * p_node);
void _w_preorder_r(HDC hdc,bst_node_t * p_node);
void _w_postorder_r(HDC hdc,bst_node_t * p_node);


void _w_destroy_bst_r(bst_node_t * p_node);
void _w_shift_left_r(bst_node_t * p_node);
void _w_shift_right_r(bst_node_t * p_node);
void w_bst_shift_left(bst_t * p_bst);
void w_bst_shift_right(bst_t * p_bst);
int MIN (int X, int Y);
int MAX (int X, int Y); 
bool w_PrintBstNode(HDC hdc,int iXCenter,int iYCenter,int radius,char * message);


///Newly added Feature////

bool DrawCircle(HDC hdc,int xCenter,int yCenter,int radius);
bool DrawArtist(HINSTANCE hInst,HDC hdc,int xCenter,int yCenter,int radius);

void _w_bst_display_r(HINSTANCE hInst,HDC hdc,bst_node_t * p_node);
//// @Developed BY Tejas Kharche////
//// Music Visulizer ////


#include <windows.h>
#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

#define VISULIZER_RED_PEN     1
#define VISULIZER_GREEN_PEN   2
#define VISULIZER_BLUE_PEN    3
#define VISULIZER_ORAGNE_PEN  4
#define VISULIZER_CYAN_PEN    5
#define VISULIZER_MGENTA_PEN  6
#define VISULIZER_YELLOW_PEN  7
#define VISULIZER_SKYBLUE_PEN 8

#define TJ_POINT_RADIUS 2

// libpath 
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "Winmm.lib")
//#pragma comment(lib, "Gdiplus.lib")


typedef struct tagmyPoint
{
    int xPosition;
    int yPosition;
    int xVelocity;
    int yVelocity;
    //int xAcceleration;
    //int yAcceleration;
    int dead;  //by default when myPoint spawns it is not dead  
    int color;
}TJ_POINT,*PTJ_POINT,**PPTJ_POINT;

typedef struct tagVector
{
    PPTJ_POINT list;
    //tagmyPoint ** list;
	int count;
}VECTOR,*PVECTOR;



typedef int VISULIZER_COLOR;
typedef int BRUSH_SIZE;


int CLIENT_XArea,CLIENT_YArea;
int CLIENT_XMouse,CLIENT_YMouse;
int CLINET_CHAR_X,CLIENT_CHAR_Y;

int MUSIC_PLAY_STATUS=FALSE;
int PARTICLE_EFFECT_STATUS=FALSE;

VISULIZER_COLOR VIS_COLOR;
int CHANGE_IN_ANGLE=0;
int VISULIZER_CHOICE=0;


//This variable manga the location and size of the Visulization effect
int VISULIZER_CENTER_X;//=CLIENT_XArea/2;
int VISULIZER_CENTER_Y;//=CLIENT_YArea/2;
int VISULIZER_RADIUS=100;

int SPAWN_RATE=50;
int particle_count=0;
PVECTOR PointVector;

//////////////////////////////// Function Declartions ///////////////////

void Visulize(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1 ,char * cMessage2,VISULIZER_COLOR iColor);
void Visulize1(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1,char * cMessage2,int AngleDeviationFactor);
void Visulize2(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1,char * cMessage2,int AngleDeviationFactor);
void Visulize3(HDC hdc,PVECTOR p_vector,int radius,VISULIZER_COLOR iColor);

PVECTOR CreateVector(void);
int PushElement(PVECTOR pVector,PTJ_POINT pPoint);
void DestroyVector(PVECTOR pVector);
void Advance(PTJ_POINT pPoint);
void Accelerate(PTJ_POINT pPoint,int xAccel,int yAccel);
void AdvanceLoop(PVECTOR pVector);
void SpawnLoop(PVECTOR pVector);
void PopElement(PVECTOR pVector,int index);
bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc);
void RandomPen(HPEN * out_hpen,COLORREF * out_color,BRUSH_SIZE brushSize);
double getDistance(int x1,int y1, int x2,int y2 );

///$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$///


///$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$///


//2. Hollywood Principal
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);




//3
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow) //ncmwindow show the window on how it should be shown e.g. maximized minimized or hidden etc.
{

    //My Addon
	 PointVector=CreateVector();  //Create Point Vector
	 
	 
	//4. Data
	WNDCLASSEX wndclass; //  style
	HWND hwnd;	// Handle == Pointer to Window
	MSG msg;	//Current msg
	TCHAR szAppName[] = TEXT("MSTC"); // name of class 
    TCHAR szWindowCaption[] = TEXT("104_Tejas : BASIC Music Visulizer SYSTEM");

	//5. Code : Fill the struct 
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0; // additional
	wndclass.cbWndExtra = 0; // additional

	wndclass.lpfnWndProc = WndProc; // it actully funcptr
	wndclass.hInstance = hInstance; // 1st param

	wndclass.hIcon = LoadIcon(NULL, IDI_QUESTION); // Custom icon for Taskbar
	wndclass.hIconSm = LoadIcon(NULL, IDI_SHIELD); // minimize icon for window itself
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	wndclass.lpszClassName = szAppName; // Group Name ""
	wndclass.lpszMenuName = NULL; // used in project

	// 6
	RegisterClassEx(&wndclass);

	// 7  Create window and return its
	hwnd = CreateWindow(szAppName, // class style
        szWindowCaption,    //Windows Caption name
		WS_OVERLAPPEDWINDOW,	   // Style of wnd
		CW_USEDEFAULT,				// x cord
		CW_USEDEFAULT,				// y cord
		CW_USEDEFAULT,				// width
		CW_USEDEFAULT,				// height
		(HWND)NULL,                 // hWndParent
        (HMENU)NULL,				// hMenu
		hInstance,					// 1st 
		NULL);						// No lpParam (used for additional data)

	// 8  memset
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd); // VIMP

	// 9. Message Loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	DestroyVector(PointVector); //Destroy Point Vector
	PointVector=NULL;
	return((int)msg.wParam);
}


// 11 a 
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// 11 b

    
    static int cxClient, cyClient, cxChar, cyChar, cxMouse,cyMouse;
	static VISULIZER_COLOR color_choice=0;
    HDC hdc = NULL; 

    TEXTMETRIC tm; 
    PAINTSTRUCT ps; 
	int xAccel;
    int yAccel;
	int pointIndex;

    switch (iMsg)
	{
		// very 1st Msg
	case WM_CREATE:
		MessageBox(NULL, TEXT("Window Created!"), TEXT("BASIC Music Visulizer SYSTEM"), MB_OK);
	       	SetTimer(hwnd, 0,33, NULL);
            hdc = GetDC(hwnd); 
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; 
            cyChar = tm.tmHeight + tm.tmExternalLeading; 
			
			CLINET_CHAR_X=cxChar;
			CLIENT_CHAR_Y=cyChar;
			
            ReleaseDC(hwnd, hdc); 
            hdc = NULL; 
    	break;

        case WM_SIZE: 
            cxClient = LOWORD(lParam); 
            cyClient = HIWORD(lParam); 
            CLIENT_XArea = cxClient;
            CLIENT_YArea = cyClient;
			VISULIZER_CENTER_X=CLIENT_XArea/2;
			VISULIZER_CENTER_Y=CLIENT_YArea/2;
            break; 


        case WM_MOUSEMOVE:
            cxMouse = LOWORD(lParam);
            cyMouse = HIWORD(lParam);
			CLIENT_XMouse=cxMouse;
			CLIENT_YMouse=cyMouse;
            
            break;

        case WM_PAINT: 
            hdc = BeginPaint(hwnd, &ps);
			
			//Visulize(hdc,cxClient/2, cyClient/2-(cyChar/2),100, "Anohana Piano OST", NULL,VIS_COLOR);
			
			/*
			if(PARTICLE_EFFECT_STATUS == TRUE)
			{
				for(pointIndex=0; pointIndex < PointVector->count; pointIndex++)
				{
					MyCircle( PointVector->list[pointIndex]->xPosition, PointVector->list[pointIndex]->yPosition, TJ_POINT_RADIUS , hdc);	
				}
			}
			*/
			
			if(VISULIZER_CHOICE == 0)
				{
					Visulize(hdc,VISULIZER_CENTER_X, VISULIZER_CENTER_Y-(cyChar/2),100, "Anohana Piano OST", NULL,VIS_COLOR);
				}
				else if(VISULIZER_CHOICE == 1)
				{
					Visulize1(hdc,VISULIZER_CENTER_X, VISULIZER_CENTER_Y-(cyChar/2),100, "Anohana Piano OST", NULL,CHANGE_IN_ANGLE);
				}else if(VISULIZER_CHOICE == 2)
				{
					Visulize2(hdc,VISULIZER_CENTER_X, VISULIZER_CENTER_Y-(cyChar/2),100, "Anohana Piano OST", NULL,CHANGE_IN_ANGLE);
				}else if(VISULIZER_CHOICE == 3)
				{
					Visulize3(hdc,PointVector,TJ_POINT_RADIUS, VIS_COLOR);
				}
				
			
			
			EndPaint(hwnd, &ps); 
            hdc = NULL; 
            break;

		case WM_KEYDOWN:
			if(wParam == 'S' || wParam == 's')
			{
				if(CHANGE_IN_ANGLE == 5 )  //Max 5 key stokes allowd after that speed will be reset
				{
					CHANGE_IN_ANGLE=1; //reset deviation
				}else{
					CHANGE_IN_ANGLE++;
				}
			
			}
			
			if(wParam == 'V' || wParam == 'v')
			{
				VISULIZER_CHOICE+=1;
				
				if(VISULIZER_CHOICE == 4)
				{
					VISULIZER_CHOICE=0;  //reset the visulizer
				}
				
				
			}
			
			if(wParam == 'M' || wParam == 'm')
			{
				//Togle code
				if(MUSIC_PLAY_STATUS==TRUE)
				{
					MUSIC_PLAY_STATUS=FALSE;
					//CHANGE_IN_ANGLE=1;
					PlaySound(NULL, NULL, SND_FILENAME | SND_ASYNC  );
				}
				else
				{
					MUSIC_PLAY_STATUS=TRUE;
					PlaySound(TEXT("music_Anohana.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP );
				}
			}
			
			if(wParam == 'P' || wParam == 'p')
			{
				if(PARTICLE_EFFECT_STATUS==TRUE)
				{
					PARTICLE_EFFECT_STATUS=FALSE;
				}
				else
				{
					PARTICLE_EFFECT_STATUS=TRUE;
				}
			}
			
			if(wParam == 'C' || wParam == 'c')
			{
				color_choice++;
				switch(color_choice)
				{
					case VISULIZER_RED_PEN:
						VIS_COLOR = VISULIZER_RED_PEN;
						break;
					case VISULIZER_GREEN_PEN:
						VIS_COLOR = VISULIZER_GREEN_PEN;
						break;
					case VISULIZER_BLUE_PEN:
						VIS_COLOR = VISULIZER_BLUE_PEN;
						break;
					case VISULIZER_YELLOW_PEN:
						VIS_COLOR = VISULIZER_YELLOW_PEN;
						break;
					case VISULIZER_ORAGNE_PEN:
						VIS_COLOR = VISULIZER_ORAGNE_PEN;
						break;
					case VISULIZER_CYAN_PEN:
						VIS_COLOR = VISULIZER_CYAN_PEN;
						break;
					case VISULIZER_MGENTA_PEN:
						VIS_COLOR = VISULIZER_MGENTA_PEN;
						break;
					case VISULIZER_SKYBLUE_PEN:
						VIS_COLOR = VISULIZER_SKYBLUE_PEN;
						break;
					default:
						VIS_COLOR = color_choice;
						color_choice=0;
						break;
					
				}
				//if( color_choice == VISULIZER_RED_PEN )
				//	VIS_COLOR = VISULIZER_RED_PEN;
			}
			break;

     case WM_TIMER:
    {
	if(VISULIZER_CHOICE == 3 || PARTICLE_EFFECT_STATUS == TRUE)
		{
			SpawnLoop(PointVector);	
			AdvanceLoop(PointVector);
        }
		InvalidateRect(hwnd, NULL, TRUE);
        return 0;
    }


	case WM_DESTROY:
		MessageBox(NULL, TEXT("Window Destroyed"), TEXT("BASIC Music Visulizer SYSTEM Going"), MB_ICONERROR);
		PostQuitMessage(0);
		break;
	}

	// 12
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}




void Visulize(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1,char * cMessage2,VISULIZER_COLOR iColor )
{
	
	COLORREF red=RGB(255,0,0); 
    COLORREF green=RGB(0,255,0); 
    COLORREF blue=RGB(0,0,255); 
	COLORREF orange=RGB(255,165,0); 
    COLORREF cyan=RGB(0,255,255); 
    COLORREF magenta=RGB(255,0,255); 
	COLORREF yellow=RGB(255,215,0); 
	COLORREF skyblue=RGB(0,191,255); 
	
	
	COLORREF textColor;
	HPEN hPenOld;
	HPEN hLinePen;
	
	switch(iColor)
	{
		case VISULIZER_RED_PEN :  
			hLinePen=CreatePen(PS_SOLID, 3 , red);
			textColor=red;
			break;
		case VISULIZER_GREEN_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , green);
			textColor=green;
			break;
		case VISULIZER_BLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , blue);
			textColor=blue;
			break;
		case VISULIZER_ORAGNE_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , orange);
			textColor=orange;
			break;
		case VISULIZER_CYAN_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , cyan);
			textColor=cyan;
			break;
		case VISULIZER_MGENTA_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , magenta);
			textColor=magenta;
			break;
		case VISULIZER_YELLOW_PEN:
			hLinePen=CreatePen(PS_SOLID, 3 , yellow);
			textColor=yellow;
			break;
		case VISULIZER_SKYBLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, 3 , skyblue);
			textColor=skyblue;
			break;
		default:
			hLinePen=CreatePen(PS_SOLID, 3 , orange);
			textColor=orange;
	} 
	
	
	
	if(MUSIC_PLAY_STATUS==TRUE)
	{
		SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
		SetBkMode(hdc,TRANSPARENT);
		SetTextColor(hdc,textColor); 
        TextOut(hdc, iCenterX,(iCenterY-CLIENT_CHAR_Y), "Music Playing", lstrlen("Music Playing"));
		
		if(cMessage1 != NULL)
		{
			TextOut(hdc, iCenterX,iCenterY, cMessage1, lstrlen(cMessage1)); 
		}
	}
	
	/*
    if(cMessage2!= NULL)  //Future scope
	{
		SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
        TextOut(hdc, iCenterX,(iCenterY-CLIENT_CHAR_Y), cMessage2, lstrlen(cMessage2)); 
	}*/
	
	
	int angle_deg; //0 to 360
    int iPointX,iPointY,iPointExternalX,iPointExternalY;
    double angle_rad;   
	
	hPenOld=(HPEN)SelectObject(hdc,hLinePen);  
	
    int External_line;
	
    for(angle_deg=0;angle_deg<=360;angle_deg+=3)
    {
        angle_rad=M_PI/180*angle_deg;
        
		//iPointY= sin(angle_rad) * iRadius;
        //iPointX= cos(angle_rad) * iRadius;
		
		iPointX= cos(angle_rad) * (iRadius);
		iPointY= sin(angle_rad) * (iRadius);
        
		
		if(MUSIC_PLAY_STATUS==FALSE)
		{
			//SetPixel(hdc,(iCenterX+iPointX),(iCenterY+iPointY),color1);
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,(iPointX+iCenterX),(iPointY+iCenterY));
		}else if(MUSIC_PLAY_STATUS==TRUE)
		{
			External_line=rand()%100;
			iPointExternalX=cos(angle_rad) * (iRadius+External_line);
			iPointExternalY=sin(angle_rad) * (iRadius+External_line);
				
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,iPointExternalX+iCenterX,iPointExternalY+iCenterY);
		}
			
    }
	
	//release Brushes   for performance bossting I think we can make them persistent and at the ned of program release them
	SelectObject(hdc,hPenOld);
	DeleteObject(hLinePen);
}



void Visulize1(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1,char * cMessage2,int AngleDeviationFactor)
{
	HPEN hPenOld;
	COLORREF textColor;
	HPEN hLinePen;
	static int deviation=1;
	
	/*
	if(deviation == 60 )
	{
		deviation=0; //reset deviation
	}else{
		deviation+=AngleDeviationFactor;
	}
	*/
	
	
	
	if(MUSIC_PLAY_STATUS==TRUE)
	{			
		if(deviation == 60 )
			{
				deviation=0; //reset deviation
		
				//delta_redius1=Radius_Factor;
				//delta_redius2=2*Radius_Factor;
	
			}else{
				deviation+=AngleDeviationFactor;
				//delta_redius1+=Radius_Factor;
				//delta_redius2+=(2*Radius_Factor);
				}
						
	}else if (MUSIC_PLAY_STATUS==FALSE)
		{
				deviation=0;
		}
	
	/*
    if(cMessage2!= NULL)  //Future scope
	{
		SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
        TextOut(hdc, iCenterX,(iCenterY-CLIENT_CHAR_Y), cMessage2, lstrlen(cMessage2)); 
	}*/
	
	
	
	int angle_deg; //0 to 360
    int iPointX,iPointY,iPointExternalX,iPointExternalY;
    double angle_rad;   
	
	//hPenOld=(HPEN)SelectObject(hdc,hLinePen);  
	
    int External_line;
	int delta_redius1=10;
	int delta_redius2=20;
	/*
    for(angle_deg=0;angle_deg<=360;angle_deg+=3)
    {
        angle_rad=M_PI/180*angle_deg;
        
		//iPointY= sin(angle_rad) * iRadius;
        //iPointX= cos(angle_rad) * iRadius;
		
		iPointX= cos(angle_rad) * (iRadius);
		iPointY= sin(angle_rad) * (iRadius);
        
		
		if(MUSIC_PLAY_STATUS==FALSE)
		{
			//SetPixel(hdc,(iCenterX+iPointX),(iCenterY+iPointY),color1);
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,(iPointX+iCenterX),(iPointY+iCenterY));
		}else if(MUSIC_PLAY_STATUS==TRUE)
		{
			External_line=rand()%100;
			iPointExternalX=cos(angle_rad) * (iRadius+External_line);
			iPointExternalY=sin(angle_rad) * (iRadius+External_line);
				
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,iPointExternalX+iCenterX,iPointExternalY+iCenterY);
		}
			
    }
	
	*/
	
	
	 	
	 POINT p1,p2,p3,p4,p5,p6;
	 
	 POINT p11,p12,p13,p14,p15,p16;
	 
	 POINT p21,p22,p23,p24,p25,p26;
	 
	 /*
	 p1.x=iRadius*cos(M_PI/6)+iCenterX; //30
	 p1.y=iRadius*sin(M_PI/6)+iCenterY;
	 
	 p2.x=iRadius*cos(3*M_PI/6)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p2.y=iRadius*sin(3*M_PI/6)+iCenterY;  
	 
	 p3.x=iRadius*cos(5*M_PI/6)+iCenterX; 
	 p3.y=iRadius*sin(5*M_PI/6)+iCenterY;
	 
	 p4.x=iRadius*cos(7*M_PI/6)+iCenterX;
	 p4.y=iRadius*sin(7*M_PI/6)+iCenterY;
	 
	 p5.x=iRadius*cos(9*M_PI/6)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p5.y=iRadius*sin(9*M_PI/6)+iCenterY;
	 
	 p6.x=iRadius*cos(11*M_PI/6)+iCenterX;
	 p6.y=iRadius*sin(11*M_PI/6)+iCenterY;
	 */
	 
	 
	 p1.x=iRadius*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p1.y=iRadius*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p2.x=iRadius*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p2.y=iRadius*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p3.x=iRadius*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p3.y=iRadius*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p4.x=iRadius*cos((210+deviation)*M_PI/180)+iCenterX;
	 p4.y=iRadius*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p5.x=iRadius*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p5.y=iRadius*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p6.x=iRadius*cos((330+deviation)*M_PI/180)+iCenterX;
	 p6.y=iRadius*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 
	 //////////////////////////////////////////////////////////////////////////////
	 
	 
	 
	 
	 
	 p11.x=(iRadius+delta_redius1)*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p11.y=(iRadius+delta_redius1)*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p12.x=(iRadius+delta_redius1)*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p12.y=(iRadius+delta_redius1)*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p13.x=(iRadius+delta_redius1)*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p13.y=(iRadius+delta_redius1)*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p14.x=(iRadius+delta_redius1)*cos((210+deviation)*M_PI/180)+iCenterX;
	 p14.y=(iRadius+delta_redius1)*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p15.x=(iRadius+delta_redius1)*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p15.y=(iRadius+delta_redius1)*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p16.x=(iRadius+delta_redius1)*cos((330+deviation)*M_PI/180)+iCenterX;
	 p16.y=(iRadius+delta_redius1)*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 //////////////////////////////////////////////////////////////////////////////////////
	 
	 p21.x=(iRadius+delta_redius2)*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p21.y=(iRadius+delta_redius2)*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p22.x=(iRadius+delta_redius2)*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p22.y=(iRadius+delta_redius2)*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p23.x=(iRadius+delta_redius2)*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p23.y=(iRadius+delta_redius2)*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p24.x=(iRadius+delta_redius2)*cos((210+deviation)*M_PI/180)+iCenterX;
	 p24.y=(iRadius+delta_redius2)*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p25.x=(iRadius+delta_redius2)*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p25.y=(iRadius+delta_redius2)*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p26.x=(iRadius+delta_redius2)*cos((330+deviation)*M_PI/180)+iCenterX;
	 p26.y=(iRadius+delta_redius2)*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 
	 
	 
	 //////////////////////////////////////////////////////////
	 
	 //Plot Triangle 1
	 
	 //Plot Line 1   p1-p5
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p11.x),(p11.y),NULL);
	 LineTo(hdc,p15.x,p15.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p21.x),(p21.y),NULL);
	 LineTo(hdc,p25.x,p25.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 //Plot Line 2   p1-p3
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p3.x,p3.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p11.x),(p11.y),NULL);
	 LineTo(hdc,p13.x,p13.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p21.x),(p21.y),NULL);
	 LineTo(hdc,p23.x,p23.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 //Plot Line 3   p3-p5
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p3.x),(p3.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p13.x),(p13.y),NULL);
	 LineTo(hdc,p15.x,p15.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p23.x),(p23.y),NULL);
	 LineTo(hdc,p25.x,p25.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 /////////////////////////////////////
	 
	 //Plot Triangle 2
	 
	 //Plot Line 1   p2-p6
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p12.x),(p12.y),NULL);
	 LineTo(hdc,p16.x,p16.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p22.x),(p22.y),NULL);
	 LineTo(hdc,p26.x,p26.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 //Plot Line 2   p2-p4
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p4.x,p4.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p12.x),(p12.y),NULL);
	 LineTo(hdc,p14.x,p14.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p22.x),(p22.y),NULL);
	 LineTo(hdc,p24.x,p24.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 //Plot Line 3   p4-p6
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p4.x),(p4.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p14.x),(p14.y),NULL);
	 LineTo(hdc,p16.x,p16.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p24.x),(p24.y),NULL);
	 LineTo(hdc,p26.x,p26.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
}



void Visulize2(HDC hdc,int iCenterX, int iCenterY ,int iRadius,char * cMessage1,char * cMessage2,int AngleDeviationFactor)
{
	HPEN hPenOld;
	COLORREF textColor;
	HPEN hLinePen;
	int Radius_Factor=1;
	
	static int isMaxLimitReached=0;
	static int isMaxSpeedLimitReached=0;
	static int deviation=0;
	
	static int delta_redius1=Radius_Factor;
	static int delta_redius2=2*Radius_Factor;
	
	if(MUSIC_PLAY_STATUS==TRUE)
	{	
		
			
		if(isMaxLimitReached == 0)
			{
	
				if(delta_redius1 == 50)
				{
					isMaxLimitReached=1;
				}
				else{
					delta_redius1+=Radius_Factor;
					delta_redius2+=(2*Radius_Factor);
					}
			}else if(isMaxLimitReached == 1)
				{
					if(delta_redius1 == 0)
					{
						isMaxLimitReached=0;
					}
					else{
						delta_redius1-=Radius_Factor;
						delta_redius2-=(2*Radius_Factor);
						}
				}
				
				
		if(deviation == 60 )
			{
				deviation=0; //reset deviation
		
				//delta_redius1=Radius_Factor;
				//delta_redius2=2*Radius_Factor;
	
			}else{
				deviation+=AngleDeviationFactor;
				//delta_redius1+=Radius_Factor;
				//delta_redius2+=(2*Radius_Factor);
				}
				
				
	}else if (MUSIC_PLAY_STATUS==FALSE)
		{
		//Aftermusic stops the lines should contract from their esisting position to Initial position
				if(delta_redius1!=0){
						delta_redius1-=Radius_Factor;
						delta_redius2-=(2*Radius_Factor);
						}
				
				deviation=0;
				/*  Smoothe roll back
				if(deviation>=0)
				{
					deviation-=3*AngleDeviationFactor;
				}					
				*/
		}
	/*
	if(deviation == 60 )
	{
		deviation=0; //reset deviation
		
				//delta_redius1=Radius_Factor;
				//delta_redius2=2*Radius_Factor;
	
	}else{
				deviation+=AngleDeviationFactor;
				//delta_redius1+=Radius_Factor;
				//delta_redius2+=(2*Radius_Factor);
			}
			
	*/
	/*
    if(cMessage2!= NULL)  //Future scope
	{
		SetTextAlign(hdc, TA_CENTER | TA_CENTER); 
        TextOut(hdc, iCenterX,(iCenterY-CLIENT_CHAR_Y), cMessage2, lstrlen(cMessage2)); 
	}*/
	
	
	
	int angle_deg; //0 to 360
    int iPointX,iPointY,iPointExternalX,iPointExternalY;
    double angle_rad;   
	
	//hPenOld=(HPEN)SelectObject(hdc,hLinePen);  
	
    int External_line;
	
	//int delta_redius1=10;
	//int delta_redius2=20;
	
	/*
    for(angle_deg=0;angle_deg<=360;angle_deg+=3)
    {
        angle_rad=M_PI/180*angle_deg;
        
		//iPointY= sin(angle_rad) * iRadius;
        //iPointX= cos(angle_rad) * iRadius;
		
		iPointX= cos(angle_rad) * (iRadius);
		iPointY= sin(angle_rad) * (iRadius);
        
		
		if(MUSIC_PLAY_STATUS==FALSE)
		{
			//SetPixel(hdc,(iCenterX+iPointX),(iCenterY+iPointY),color1);
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,(iPointX+iCenterX),(iPointY+iCenterY));
		}else if(MUSIC_PLAY_STATUS==TRUE)
		{
			External_line=rand()%100;
			iPointExternalX=cos(angle_rad) * (iRadius+External_line);
			iPointExternalY=sin(angle_rad) * (iRadius+External_line);
				
			MoveToEx(hdc,(iPointX+iCenterX),(iPointY+iCenterY),NULL);
			LineTo(hdc,iPointExternalX+iCenterX,iPointExternalY+iCenterY);
		}
			
    }
	
	*/
	
	
	 	
	 POINT p1,p2,p3,p4,p5,p6;
	 
	 POINT p11,p12,p13,p14,p15,p16;
	 
	 POINT p21,p22,p23,p24,p25,p26;
	 
	 
	 p1.x=iRadius*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p1.y=iRadius*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p2.x=iRadius*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p2.y=iRadius*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p3.x=iRadius*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p3.y=iRadius*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p4.x=iRadius*cos((210+deviation)*M_PI/180)+iCenterX;
	 p4.y=iRadius*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p5.x=iRadius*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p5.y=iRadius*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p6.x=iRadius*cos((330+deviation)*M_PI/180)+iCenterX;
	 p6.y=iRadius*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 
	 //////////////////////////////////////////////////////////////////////////////
	 
	 
	 
	 
	 
	 p11.x=(iRadius+delta_redius1)*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p11.y=(iRadius+delta_redius1)*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p12.x=(iRadius+delta_redius1)*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p12.y=(iRadius+delta_redius1)*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p13.x=(iRadius+delta_redius1)*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p13.y=(iRadius+delta_redius1)*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p14.x=(iRadius+delta_redius1)*cos((210+deviation)*M_PI/180)+iCenterX;
	 p14.y=(iRadius+delta_redius1)*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p15.x=(iRadius+delta_redius1)*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p15.y=(iRadius+delta_redius1)*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p16.x=(iRadius+delta_redius1)*cos((330+deviation)*M_PI/180)+iCenterX;
	 p16.y=(iRadius+delta_redius1)*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 //////////////////////////////////////////////////////////////////////////////////////
	 
	 p21.x=(iRadius+delta_redius2)*cos((30+deviation)*M_PI/180)+iCenterX; //30
	 p21.y=(iRadius+delta_redius2)*sin((30+deviation)*M_PI/180)+iCenterY;
	 
	 p22.x=(iRadius+delta_redius2)*cos((90+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 1/2 i kept this simple for readability :)
	 p22.y=(iRadius+delta_redius2)*sin((90+deviation)*M_PI/180)+iCenterY;  
	 
	 p23.x=(iRadius+delta_redius2)*cos((150+deviation)*M_PI/180)+iCenterX; 
	 p23.y=(iRadius+delta_redius2)*sin((150+deviation)*M_PI/180)+iCenterY;
	 
	 p24.x=(iRadius+delta_redius2)*cos((210+deviation)*M_PI/180)+iCenterX;
	 p24.y=(iRadius+delta_redius2)*sin((210+deviation)*M_PI/180)+iCenterY;
	 
	 p25.x=(iRadius+delta_redius2)*cos((270+deviation)*M_PI/180)+iCenterX;  //I know this can be reduced to 3/2 i kept this simple for readability :)
	 p25.y=(iRadius+delta_redius2)*sin((270+deviation)*M_PI/180)+iCenterY;
	 
	 p26.x=(iRadius+delta_redius2)*cos((330+deviation)*M_PI/180)+iCenterX;
	 p26.y=(iRadius+delta_redius2)*sin((330+deviation)*M_PI/180)+iCenterY;
	 
	 
	 
	 
	 //////////////////////////////////////////////////////////
	 
	 //Plot Triangle 1
	 
	 //Plot Line 1   p1-p5
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p15.x,p15.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p25.x,p25.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 //Plot Line 2   p1-p3
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p1.x),(p1.y),NULL);
	 LineTo(hdc,p3.x,p3.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p11.x),(p11.y),NULL);
	 LineTo(hdc,p3.x,p3.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p21.x),(p21.y),NULL);
	 LineTo(hdc,p3.x,p3.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 
	 //Plot Line 3   p3-p5
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p3.x),(p3.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p13.x),(p13.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p23.x),(p23.y),NULL);
	 LineTo(hdc,p5.x,p5.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 /////////////////////////////////////
	 
	 //Plot Triangle 2
	 
	 //Plot Line 1   p2-p6
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p16.x,p16.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p26.x,p26.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 //Plot Line 2   p2-p4
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p2.x),(p2.y),NULL);
	 LineTo(hdc,p4.x,p4.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p12.x),(p12.y),NULL);
	 LineTo(hdc,p4.x,p4.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p22.x),(p22.y),NULL);
	 LineTo(hdc,p4.x,p4.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 //Plot Line 3   p4-p6
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p4.x),(p4.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p14.x),(p14.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
	 RandomPen(&hLinePen,&textColor,3);
	 hPenOld=(HPEN)SelectObject(hdc,hLinePen); 
	 MoveToEx(hdc,(p24.x),(p24.y),NULL);
	 LineTo(hdc,p6.x,p6.y);
	 SelectObject(hdc,hPenOld);
	 DeleteObject(hLinePen);
	 
}





void RandomPen(HPEN * out_hpen,COLORREF * out_color,BRUSH_SIZE brushSize)
{
	
	COLORREF red=RGB(255,0,0); 
    COLORREF green=RGB(0,255,0); 
    COLORREF blue=RGB(0,0,255); 
	COLORREF orange=RGB(255,165,0); 
    COLORREF cyan=RGB(0,255,255); 
    COLORREF magenta=RGB(255,0,255); 
	COLORREF yellow=RGB(255,215,0); 
	COLORREF skyblue=RGB(0,191,255); 
	
	COLORREF textColor;
	HPEN hLinePen;
	
	int iColor=rand()%9; //some random number
	switch(iColor)
	{
		case VISULIZER_RED_PEN :  
			hLinePen=CreatePen(PS_SOLID, brushSize , red);
			textColor=red;
			break;
		case VISULIZER_GREEN_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , green);
			textColor=green;
			break;
		case VISULIZER_BLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , blue);
			textColor=blue;
			break;
		case VISULIZER_ORAGNE_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , orange);
			textColor=orange;
			break;
		case VISULIZER_CYAN_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , cyan);
			textColor=cyan;
			break;
		case VISULIZER_MGENTA_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , magenta);
			textColor=magenta;
			break;
		case VISULIZER_YELLOW_PEN:
			hLinePen=CreatePen(PS_SOLID, brushSize , yellow);
			textColor=yellow;
			break;
		case VISULIZER_SKYBLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, brushSize , skyblue);
			textColor=skyblue;
			break;
		default:
			hLinePen=CreatePen(PS_SOLID, brushSize , orange);
			textColor=orange;
	} 
	
	*out_hpen=hLinePen;
	*out_color=textColor;
}

double getDistance(int x1,int y1, int x2,int y2 )
{
	//@ Return the distacne between 2 poitns using euclidian distance formula;
	double distacne;
	int delta_x=x2-x1;
	int delta_y=y2-y1;
	
	distacne=pow(pow(delta_x,2) + pow(delta_y,2), (0.5));
	return (distacne);
}

///$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ Visulizer 4 (Particle Effect $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$////


void Visulize3(HDC hdc,PVECTOR p_vector,int radius,VISULIZER_COLOR iColor)
{
	COLORREF red=RGB(255,0,0); 
    COLORREF green=RGB(0,255,0); 
    COLORREF blue=RGB(0,0,255); 
	COLORREF orange=RGB(255,165,0); 
    COLORREF cyan=RGB(0,255,255); 
    COLORREF magenta=RGB(255,0,255); 
	COLORREF yellow=RGB(255,215,0); 
	COLORREF skyblue=RGB(0,191,255); 
	
	
	COLORREF textColor;
	HPEN hPenOld;
	HPEN hLinePen;
	
	switch(iColor)
	{
		case VISULIZER_RED_PEN :  
			hLinePen=CreatePen(PS_SOLID, 1 , red);
			textColor=red;
			break;
		case VISULIZER_GREEN_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , green);
			textColor=green;
			break;
		case VISULIZER_BLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , blue);
			textColor=blue;
			break;
		case VISULIZER_ORAGNE_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , orange);
			textColor=orange;
			break;
		case VISULIZER_CYAN_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , cyan);
			textColor=cyan;
			break;
		case VISULIZER_MGENTA_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , magenta);
			textColor=magenta;
			break;
		case VISULIZER_YELLOW_PEN:
			hLinePen=CreatePen(PS_SOLID, 1 , yellow);
			textColor=yellow;
			break;
		case VISULIZER_SKYBLUE_PEN :
			hLinePen=CreatePen(PS_SOLID, 1 , skyblue);
			textColor=skyblue;
			break;
		default:
			hLinePen=CreatePen(PS_SOLID, 1 , orange);
			textColor=orange;
	} 
	
	hPenOld=(HPEN)SelectObject(hdc,hLinePen);
	
	int pointIndex;
	if(MUSIC_PLAY_STATUS==TRUE)
	{
		PARTICLE_EFFECT_STATUS =TRUE;
		for(pointIndex=0; pointIndex < p_vector->count; pointIndex++)
		{
			MyCircle( p_vector->list[pointIndex]->xPosition, p_vector->list[pointIndex]->yPosition, radius , hdc);	
		}
	}
	else 
	{
		PARTICLE_EFFECT_STATUS = FALSE;
	}
	
	DeleteObject(hLinePen);
	SelectObject(hdc,hPenOld);
}


PVECTOR CreateVector(void)
{
    PVECTOR pVector=NULL;
    pVector=(PVECTOR)malloc(sizeof(VECTOR));
    pVector->list=NULL;
    pVector->count=0;
    return pVector;
}

void DestroyVector(PVECTOR pVector)
{
    int i=0;
    for (i=0;i<pVector->count;i++)
    {
        free(pVector->list[i]);
		pVector->list[i]=NULL;
    }
    free(pVector->list);
    pVector->list=NULL;
    pVector->count=0;
    free(pVector);
    pVector=NULL;
}

bool MyCircle(int iXCenter,int iYCenter,int radius,HDC hdc)
{
    return(Ellipse(hdc,(iXCenter-radius),(iYCenter-radius),(iXCenter+radius),(iYCenter+radius)));
}

int PushElement(PVECTOR pVector,PTJ_POINT pPoint)
{
    pVector->count+=1;
    pVector->list=(PPTJ_POINT)realloc(pVector->list,pVector->count*sizeof(PTJ_POINT));
	
    pVector->list[pVector->count-1]=pPoint;
	
    return (EXIT_SUCCESS);
}


void PopElement(PVECTOR pVector,int index)
{
    if(index <0 || index >= pVector->count)
		return;
	
	free(pVector->list[index]);
	pVector->list[index] = NULL;
	
	int i;
	for(i=index;i< pVector->count-1 ;i++)
	{
		pVector->list[i]=pVector->list[i+1];
		pVector->list[i+1] = NULL;
	}
	
	pVector->count-=1;
}


PTJ_POINT CreatePoint(int xPos,int yPos, int xVel, int yVel)
{
    PTJ_POINT pPoint=NULL;
	pPoint=(PTJ_POINT)malloc(sizeof(TJ_POINT));
    pPoint->xPosition=xPos;
    pPoint->yPosition=yPos;
    pPoint->xVelocity=xVel;
    pPoint->yVelocity=yVel;
	pPoint->dead=0;

    return pPoint;
}


void Accelerate(PTJ_POINT pPoint,int xAcc,int yAcc)
{
    pPoint->xVelocity+=xAcc;
    pPoint->yVelocity+=yAcc;
}


void Advance(PTJ_POINT pPoint){
    pPoint->xPosition+=pPoint->xVelocity;
    pPoint->yPosition+=pPoint->yVelocity;


	//below part can be put into a seperate function wich will check if the particale is out of the bounds

	if(getDistance(VISULIZER_CENTER_X,VISULIZER_CENTER_Y,pPoint->xPosition,pPoint->yPosition) >= VISULIZER_RADIUS )
	{
		pPoint->dead=1;	
	}
}	


void SpawnLoop(PVECTOR pVector)
{
    int i=0;
    int xVel;
    int yVel;
	int xAccel;
    int yAccel;
    PTJ_POINT pPoint;
    for(i=0;i<SPAWN_RATE;i++)
    {
        xVel=rand()%2;
        yVel=rand()%2;  
			
        pPoint=CreatePoint(VISULIZER_CENTER_X,VISULIZER_CENTER_Y,xVel,yVel);   //point will be created at current mouse position
        //pPoint=CreatePoint(CLIENT_XArea,CLIENT_YArea,xVel,yVel);  //All points will start from screen's center 
        PushElement(pVector,pPoint);
        particle_count+=1;  //This particle_count  is a global var which act as a Counter for total active particle which were spawned
    }
	pVector=NULL;
}


void AdvanceLoop(PVECTOR pVector)
{
	static int accel_toggle=0;
	
    int i=0;
    int xAccel;
    int yAccel;
    //PTJ_POINT pPoint;
	
    for (i=0;i<pVector->count;i++)
    {
        //if(PointVector->list[i]==NULL)
		//continue;
        xAccel=rand()%5;
        yAccel=rand()%5;
		
		//xAccel=((rand()%2==0)?xAccel:-1*xAccel);
		//yAccel=((rand()%2!=0)?yAccel:-1*yAccel);
		
		/// Togling the accelration direction
		if(accel_toggle == 0)
		{
			accel_toggle=1;
			xAccel=1*xAccel;
			yAccel=-1*yAccel;
			
		}else if(accel_toggle ==1)
		{
			xAccel=-1*xAccel;
			yAccel=1*yAccel;
			accel_toggle=0;
		}
		
	
		Advance(pVector->list[i]);
		
		Accelerate(pVector->list[i],xAccel,yAccel);
		
        if(pVector->list[i]->dead==1)
        {
            PopElement(pVector,i);
            particle_count-=1;
        }

    } 
}

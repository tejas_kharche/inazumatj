//#### My Grep Implementation #####
//#### By Tejas Kharche ####


#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>


#ifndef size_t
#define size_t unsigned long 
#endif

#define SUCCESS 1
#define FAILURE 0
#define STRING_PATTERN_NOT_FOUND 2


int mygetline(char **line ,size_t * char_cnt, FILE * fp);

int main(int argc,char * argv[]){
 //Schema:  Singer,Song Name, Movie
     
 	FILE * fp=fopen(argv[1],"rb+");
   
	char * ret=NULL;
	char * line=NULL;
   char * pattern=argv[2];
   size_t line_size;
   
   while(mygetline(&line,&line_size,fp)!=-1)
     {
     	ret = strstr(line, pattern);
     	if(ret != NULL) 
	{
   	   //fprintf(stdout,"%s",ret);  //this will not return entire match but all character in between match till new line char
	   fprintf(stdout,"%s",line);

   	}
     	free(line);
     	line=NULL;
     }
	fclose(fp);

 return(EXIT_SUCCESS);
}



int mygetline(char **line ,size_t * char_cnt, FILE * fp){

	
	long int BUFF_SIZE=64;
	long int BUFF_INCREMENT=64;
	
	long int FILE_READ_POINT=0;
	long int FILE_CHAR_COUNT=0;

	fseek(fp,0,SEEK_CUR);
	FILE_READ_POINT=ftell(fp);
	
	int i;
	int LINE_END=0;
	
	
	fseek(fp,FILE_READ_POINT,SEEK_SET);
	 

	 
	 while(LINE_END!=1)
	 {
	 	fseek(fp,FILE_READ_POINT,SEEK_SET);
	 
	 BUFF_SIZE=BUFF_SIZE+BUFF_INCREMENT;
	 
	 
	 	*line=(char *)realloc(*line, sizeof(char)*BUFF_SIZE);
       
       
    if(fgets(*line,BUFF_SIZE,fp)!=NULL)
    {
       
       for(i=0;i<(BUFF_SIZE-1) -1;i++)
       {
   
       	if((*line)[i]=='\0' )
       	{
       	       LINE_END=1;
       	       *char_cnt=BUFF_SIZE;
       	       break;
       	}
       	//puts(" Inside For Loop ");
       }
      
      //BUFF_MULTIPLIER++;
    }else  //this case handles last line in file
    {
    	  LINE_END=1;
    	  *char_cnt=0;
    }
	 }
	 
	 fseek(fp,0,SEEK_CUR);
	FILE_READ_POINT=ftell(fp);
      
      //to vheck if we reached file end
      fseek(fp,0,SEEK_END);
      FILE_CHAR_COUNT=ftell(fp);
      
      //printf(" TOTAL CHAR %ld  CURENT READ %ld\n",FILE_CHAR_COUNT,FILE_READ_POINT);
      
      if(FILE_READ_POINT==FILE_CHAR_COUNT)
      {
           return (-1);
      }else
      {
          fseek(fp,FILE_READ_POINT,SEEK_SET);
          return (0);
      }
      
}


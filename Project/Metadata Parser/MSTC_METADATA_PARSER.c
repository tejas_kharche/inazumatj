//#### My Grep Implementation #####
//#### By Tejas Kharche ####


#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>

#define SUCCESS 1
#define FAILURE 0
#define STRING_PATTERN_NOT_FOUND 2

#ifndef size_t
#define size_t unsigned long 
#endif

int mygetline(char **line ,size_t * char_cnt, FILE * fp);
void displayToken(char * token);
void getColumn(char * fileName,char * filedSeperator,int columnNumber,char * searchPattern);


void getSongsByArtist(char * ArtistName);
void getSongsByMusicDirector(char * MusicDirector);
void getSongsByAlbum(char * Ablum);
void getSongsByTrackName(char * TrackName);

int main(int argc,char * argv[]){
 //Schema:  Singer,Song Name, Movie
  
 puts("Get All Songs By ArtistName(Coldplay)");
 getSongsByArtist("Coldplay");
 
 puts("########################");
 
 puts("Get All Songs By ArtistName(Lata Mangeshkar)");
 getSongsByArtist("Lata Mangeshkar");
 
 puts("########################");
 
 puts("Get All Songs By Music Director(Pritam)");
 getSongsByMusicDirector("Pritam");
 
 puts("########################");
 
 puts("Get All Songs By Music Director(Mithoon)");
 getSongsByMusicDirector("Mithoon");
 
 puts("########################");
 
 puts("Get All Songs By Album(Konvicted)");
 getSongsByAlbum("Konvicted");
 
 return(EXIT_SUCCESS);
}




int mygetline(char **line ,size_t * char_cnt, FILE * fp){

	
	long int BUFF_SIZE=64;
	long int BUFF_INCREMENT=64;
	
	long int FILE_READ_POINT=0;
	long int FILE_CHAR_COUNT=0;

	fseek(fp,0,SEEK_CUR);
	FILE_READ_POINT=ftell(fp);
	
	int i;
	int LINE_END=0;
	
	
	fseek(fp,FILE_READ_POINT,SEEK_SET);
	 

	 
	 while(LINE_END!=1)
	 {
	 	fseek(fp,FILE_READ_POINT,SEEK_SET);
	 
	 BUFF_SIZE=BUFF_SIZE+BUFF_INCREMENT;
	 
	 
	 	*line=(char *)realloc(*line, sizeof(char)*BUFF_SIZE);
       
       
    if(fgets(*line,BUFF_SIZE,fp)!=NULL)
    {
       
       for(i=0;i<(BUFF_SIZE-1) -1;i++)
       {
   
       	if((*line)[i]=='\0'  ) //This include \n or \r character to avoid it done follwoing
	//if((*line)[i]=='\0' || (*line)[i]=='\n' || (*line)[i]=='\r' )
       	{
       	       LINE_END=1;
       	       *char_cnt=BUFF_SIZE;
       	       break;
       	}
       	//puts(" Inside For Loop ");
       }
      
      //BUFF_MULTIPLIER++;
    }else  //this case handles last line in file
    {
    	  LINE_END=1;
    	  *char_cnt=0;
    }
	 }
	 
	 fseek(fp,0,SEEK_CUR);
	FILE_READ_POINT=ftell(fp);
      
      //to vheck if we reached file end
      fseek(fp,0,SEEK_END);
      FILE_CHAR_COUNT=ftell(fp);
      
      //printf(" TOTAL CHAR %ld  CURENT READ %ld\n",FILE_CHAR_COUNT,FILE_READ_POINT);
      
      if(FILE_READ_POINT==FILE_CHAR_COUNT)
      {
           return (-1);
      }else
      {
          fseek(fp,FILE_READ_POINT,SEEK_SET);
          return (0);
      }
      
}

void displayToken(char * token)
{
  //puts("Inside Display function");
  int i=0;
  size_t token_len=strlen(token);
  for(i=0;i<token_len;i++)
  {
      if(token[i]!='\n')
	 printf("%c",token[i]);
      else
      {
	 break;
      }
  }
  puts("");
}


void getColumn(char * fileName,char * filedSeperator,int columnNumber,char * searchPattern)
{
   FILE * fp=fopen(fileName,"rb+");
   char * ret=NULL;
   char * line=NULL;

   //printf("filed Choosen is : %d \n",columnNumber);
   char * token;

   size_t line_size;
   
   while(mygetline(&line,&line_size,fp)!=-1)
     {
     	ret = strstr(line, searchPattern);
     	if(ret != NULL) 
	{
   	   //fprintf(stdout,"%s",ret);
	   int field_run=0;
	   
	  
	   token=strtok(line,filedSeperator);
	   
	   while(token!=NULL)
	   {
		field_run++;
		if(field_run==columnNumber)
		{
	           //printf("Field_RUN : %d FIELD : %d\n",field_run,columnNumber); 
		   //fprintf(stdout,"%s\n",token);
		   displayToken(token);

		   break;
		}
		//puts("While LOOP");	
		token=strtok(NULL,filedSeperator);
	   
	   }
   	}
	
     	free(line);
     	line=NULL;
    
     }//End of mygetline() while loop
   fclose(fp);
}


void getSongsByArtist(char * ArtistName)
{
	getColumn("Metadata.csv",",",2,ArtistName);
}

void getSongsByMusicDirector(char * MusicDirector)
{
	getColumn("Metadata.csv",",",2,MusicDirector);
}

void getSongsByAlbum(char * Ablum)
{
	getColumn("Metadata.csv",",",2,Ablum);
}

void getSongsByTrackName(char * TrackName)
{
	getColumn("Metadata.csv",",",2,TrackName);
}
